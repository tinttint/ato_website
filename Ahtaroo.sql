-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 31, 2017 at 04:58 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ahtaroo`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `MainTypeID` int(11) NOT NULL,
  `CategoryName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `MainTypeID`, `CategoryName`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'AccessControl', 0, '2017-07-10 21:11:16', '2017-07-10 21:11:16'),
(2, 1, 'CCTV System', 0, '2017-07-10 21:13:00', '2017-07-10 21:13:00'),
(3, 1, 'FireAlarm System', 0, '2017-07-10 21:13:10', '2017-07-10 21:13:10'),
(4, 2, 'Hotel PMS Software', 0, '2017-07-10 21:13:22', '2017-07-10 21:13:22'),
(5, 2, 'Customized Software', 0, '2017-07-10 21:13:35', '2017-07-10 21:13:35'),
(6, 2, 'SAP Services and Solutions', 0, '2017-07-10 21:14:09', '2017-07-10 21:14:09');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `cname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `btype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `cname`, `btype`, `address`, `city`, `country`, `phone`, `email`, `website`, `logo`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'mm', 'mm', 'No.23 Bahan Township', 'Yangon', 'Myanmar', '09450034528', 'mm@gmail.com', 'mm.com', 'changecolor.PNG', 0, '2017-07-11 00:42:03', '2017-07-11 00:42:03'),
(2, 'mcbbank', 'Bank', 'No3 dkgjoj', 'Yangon', 'Myanmar', '0987653667', 'mcb@gmail.com', 'www.mcb.com', 'feederror.PNG', 0, '2017-07-13 01:47:48', '2017-07-13 01:47:48'),
(3, 'Toyo', 'Battery Company', 'No.23 Bahan Township', 'Yangon', 'Myanmar', '0137583', 'toyo123@gmail.com', 'toyo.com', '10314649_1182247665255390_8416416224978883708_n.jpg', 0, '2017-07-29 00:17:18', '2017-07-29 00:17:18'),
(4, 'Ailee Yoe', 'restaurant', 'No3 Mingalar town', 'Yangon', 'Myanmar', '094583245', 'aileeyoe@gmail.com', 'aileeyoe.com', 'expense.PNG', 0, '2017-07-29 00:46:23', '2017-07-29 00:46:23');

-- --------------------------------------------------------

--
-- Table structure for table `documenttype`
--

CREATE TABLE `documenttype` (
  `id` int(10) UNSIGNED NOT NULL,
  `documenttype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documenttype`
--

INSERT INTO `documenttype` (`id`, `documenttype`, `created_at`, `updated_at`) VALUES
(1, 'Brochures', '2017-07-11 04:05:00', '2017-07-11 04:06:00'),
(2, 'Data Sheet', '2017-07-11 04:10:45', '2017-07-11 04:08:23'),
(3, 'User Manual', '2017-07-13 05:47:00', '2017-07-12 17:30:00'),
(4, 'SDK', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(14, '2017_06_09_063311_create_maintype_table', 1),
(15, '2017_06_10_040216_create_category_table', 1),
(16, '2017_06_12_033414_create_product_table', 1),
(17, '2017_07_04_051608_create_support_table', 1),
(18, '2017_07_04_051841_create_document_table', 1),
(19, '2017_07_05_171456_creat_customer_table', 2),
(20, '2017_07_11_205518_create-productcustomer-table', 3),
(21, '2017_07_20_204551_create-register-table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `ProductCategoryID` int(11) NOT NULL,
  `ProductName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `ProductCategoryID`, `ProductName`, `Description`, `photo`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Finger Print', 'Finger Print touch finger', NULL, 0, '2017-07-10 21:25:21', '2017-07-10 21:25:21'),
(2, 1, 'Face Recognition', 'Face Recognition scan your face', NULL, 0, '2017-07-10 21:27:46', '2017-07-10 21:27:46'),
(3, 1, 'Hotel Door Lock System', 'Hotel Door Lock', NULL, 0, '2017-07-10 21:28:40', '2017-07-10 21:28:40'),
(4, 1, 'Hotel Safe Box System', 'Hotel Safe Box', NULL, 0, '2017-07-10 21:28:56', '2017-07-10 21:28:56'),
(5, 1, 'Gate Control and Parking Management System', 'Gate Control and Parking Management', NULL, 0, '2017-07-10 21:29:25', '2017-07-10 21:29:25'),
(6, 2, 'CCTV', 'IP CCTV and Analog CCTV monitoring and recording systems for Offices, Companies, Factories, Hotels, Airports, Seaports, Residents and Public Area security and operation purposes.', NULL, 0, '2017-07-10 21:30:29', '2017-07-10 21:31:15'),
(7, 3, 'Fire Alarm', 'Addressable Fire Alarm and Conventional Fire Alarm Systems together with Smoke Detectors, Heat Detectors, Gas Detectors, Manual Call Points, Strobes and Sounders for Offices, Companies, Factories, Hotels, Airports, Seaports, Residents and Public Area fire safety purposes.', NULL, 0, '2017-07-10 21:31:02', '2017-07-10 21:31:02'),
(8, 4, 'Hotel PMS Software', 'Hotel PMS Software', NULL, 0, '2017-07-10 21:31:44', '2017-07-10 21:31:44'),
(9, 5, 'HR Management System', 'HR Management system', NULL, 0, '2017-07-10 21:32:28', '2017-07-10 21:32:28'),
(10, 5, 'Performance Management System', 'Performance Management System', NULL, 0, '2017-07-10 21:32:45', '2017-07-10 21:32:45'),
(11, 5, 'Issue Tracking System', 'Issue Tracking System', NULL, 0, '2017-07-10 21:33:06', '2017-07-10 21:33:06'),
(12, 5, 'Cardiac Surgery Record System', 'Cardiac Surgery Record System', NULL, 0, '2017-07-10 21:33:24', '2017-07-10 21:33:24'),
(13, 5, 'Attendance Management System', 'Attendance Management System', NULL, 0, '2017-07-10 21:33:47', '2017-07-10 21:33:47'),
(14, 5, 'ERP', 'ERP', NULL, 0, '2017-07-10 21:34:01', '2017-07-10 21:34:01'),
(15, 6, 'SAP Services and Solutions', 'SAP Services and Solutions', NULL, 0, '2017-07-10 21:34:20', '2017-07-10 21:34:20');

-- --------------------------------------------------------

--
-- Table structure for table `productcustomer`
--

CREATE TABLE `productcustomer` (
  `id` int(10) UNSIGNED NOT NULL,
  `ProductID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productcustomer`
--

INSERT INTO `productcustomer` (`id`, `ProductID`, `CustomerID`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 9, 2, 0, '2017-07-13 01:48:08', '2017-07-13 01:48:08'),
(2, 5, 1, 0, '2017-07-27 03:18:07', '2017-07-27 03:18:07'),
(3, 1, 3, 0, '2017-07-29 00:17:47', '2017-07-29 00:17:47'),
(4, 12, 4, 0, '2017-07-29 00:46:48', '2017-07-29 00:46:48');

-- --------------------------------------------------------

--
-- Table structure for table `productmaintype`
--

CREATE TABLE `productmaintype` (
  `id` int(10) UNSIGNED NOT NULL,
  `MainTypeName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productmaintype`
--

INSERT INTO `productmaintype` (`id`, `MainTypeName`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Hardware', 0, '2017-07-10 21:06:42', '2017-07-10 21:06:42'),
(2, 'Software', 0, '2017-07-10 21:06:55', '2017-07-10 21:06:55');

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `org` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `support_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `name`, `email`, `org`, `phone`, `support_id`, `created_at`, `updated_at`) VALUES
(1, 'wah wah khaing', 'khainglay@gmail.com', 'bank', '097634568', 2, '2017-07-27 21:27:29', '2017-07-27 21:27:29'),
(2, 'susu', 'susu@gmail.com', 'bank', '09873629', 1, '2017-07-28 01:42:29', '2017-07-28 01:42:29'),
(3, 'daw', 'daw@gmail.com', 'sfnm, ,mnbvcx', '2345678', 3, '2017-07-28 02:32:15', '2017-07-28 02:32:15'),
(4, 'eudu', 'eudu@gmail.com', 'Battery', '93958658493', 2, '2017-07-28 02:51:26', '2017-07-28 02:51:26'),
(5, 'uidgijri', 'sdgjopse@gmail.com', 'sdigjwo', '526475867989', 2, '2017-07-28 02:59:42', '2017-07-28 02:59:42'),
(6, 'ldmg', 'dji@gmail.com', 'dogjo', '12345', 2, '2017-07-28 21:34:10', '2017-07-28 21:34:10'),
(7, 'hiiehiee', 'hie@gmail.com', 'skdsjgkw', '123456', 2, '2017-07-28 21:35:50', '2017-07-28 21:35:50');

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `id` int(10) UNSIGNED NOT NULL,
  `ProductID` int(11) NOT NULL,
  `DocumentTypeID` int(11) NOT NULL,
  `Code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `FileName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`id`, `ProductID`, `DocumentTypeID`, `Code`, `Description`, `FileName`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 'T58', 'dfiejijKNjdnisaos', '10314649_1182247665255390_8416416224978883708_n.jpg', 0, '2017-07-11 00:33:32', '2017-07-12 23:07:31'),
(2, 1, 1, 'TA500', 'Finger Print scan your finger', 'Laravel Framework Myanmar.docx', 0, '2017-07-12 23:05:41', '2017-07-12 23:05:41'),
(3, 5, 1, 'M#456', 'dgmowkoqkop', 'AppDevPresentation-Final_edited.pdf', 0, '2017-07-12 23:09:20', '2017-07-12 23:09:20'),
(4, 3, 2, 'CA200', 'sidjgjqwi3iq', 'AppDevPresentation-Final_edited.pdf', 0, '2017-07-26 22:22:52', '2017-07-26 22:22:52'),
(5, 8, 3, 'A4567', 'hsigjdaowoejwioa', 'Laravel Framework Myanmar.docx', 0, '2017-07-26 22:23:27', '2017-07-26 22:23:27'),
(6, 10, 4, 'H500', 'djoifhihawihHWIAHIU', 'Laravel Framework Myanmar.docx', 0, '2017-07-26 22:23:55', '2017-07-26 22:23:55'),
(7, 1, 1, 'yh789', 'dyhnml.ljhgfd', 'expense.PNG', 0, '2017-07-27 00:50:02', '2017-07-27 00:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documenttype`
--
ALTER TABLE `documenttype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productcustomer`
--
ALTER TABLE `productcustomer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productmaintype`
--
ALTER TABLE `productmaintype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `documenttype`
--
ALTER TABLE `documenttype`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `productcustomer`
--
ALTER TABLE `productcustomer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `productmaintype`
--
ALTER TABLE `productmaintype`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
