<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'Product';
    protected $fillable = ['ProductSubCategoryID','ProductName','Description','photo','isDeleted'];
}
