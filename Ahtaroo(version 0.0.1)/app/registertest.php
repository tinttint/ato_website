<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class registertest extends Model
{
    //
    protected $table='register';
    protected $fillable=['name','email','org','phone'];
}
