<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'Category';
    protected $fillable = ['MainTypeID','CategoryName','isDeleted'];
}
