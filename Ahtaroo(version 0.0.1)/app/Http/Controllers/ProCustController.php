<?php

namespace App\Http\Controllers;
use App\customer;
use App\product;
use DB;
use App\Http\Controllers\Controller;
use App\ProductCustomerModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ProCustController extends Controller
{
 
 public function index()
  {
 		$Task1=DB::table('product')->get();
        $Task = DB::table('customer')->get();
        return view('ProductCustomer',compact('Task','Task1'));
 } 
  public function store(Request $request)
    {	
    	$main=new ProductCustomerModel;
    	$main->ProductID=Input::get('product');
    	$main->CustomerID=Input::get('customer');
    	$main->isDeleted=Input::get('delete');
        $main->save();
        return redirect('ProductCustomerList');
        }
    public function showlist()
	{
     $Task = DB::table('productcustomer')
            ->join('product', 'productcustomer.ProductID', '=', 'product.id')
            ->join('customer','productcustomer.CustomerID','=','customer.id')
            ->select('productcustomer.*', 'product.ProductName','customer.cname')
            ->get();
    return view('ProductCustomerList',compact('Task'));
}
public function editlist($id)
    {
      $Task1=DB::table('product')->get();
      $Task=DB::table('customer')->get();
      $Edit=ProductCustomerModel::find($id);  
       return view('ProCustEdit',compact('Edit','Task1','Task'));
    }
    public function updatelist(Request $request,$id)
    {
        $Edit=ProductCustomerModel::find($id);
        $Edit->ProductID=$request->product;
        $Edit->CustomerID=$request->customer;
        $Edit->isDeleted=$request->delete;
        $Edit->save();
        return redirect ('ProductCustomerList');
    }
     public function deletelist(Request $request,$id)
    {

        $Edit=ProductCustomerModel::find($id);
        $Edit->isDeleted='1';
        $Edit->save();
        return redirect ('ProductCustomerList');
    }
   
}




