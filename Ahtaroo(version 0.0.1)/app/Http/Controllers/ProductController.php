<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Subcategory;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
class ProductController extends Controller
{
    //
     public function showdata(){
        $Task=DB::table('category')->get();
        return view('product',compact('Task'));
     }
    public function productupload(Request $request){
     $product= new Product;
     $product->ProductCategoryID=Input::get('main');
     $product->ProductName=Input::get('product');
     $product->Description=Input::get('description');
     if (Input::hasFile('image')){
      $file=Input::file('image');
      $file->move(public_path().'/photo',$file->getClientOriginalName());
      $product->photo=$file->getClientOriginalName();  
     }
     $product->isDeleted=Input::get('delete');
     $product->save();
     return redirect('list');
    }
    public function showlist(Request $request){
        $Task = DB::table('product')
            ->join('category', 'product.ProductCategoryID', '=', 'category.id')
            ->select('product.*','category.CategoryName')
            ->get();
        return view('upload',compact('Task'));
    }
    public function editproduct($id)
    {
      $Task1=DB::table('category')->get();
      $Edit=product::find($id);
      if(Input::hasFile('image')) {

                $file = Input::file('image');
                $file->move(public_path() . '/photo',$file->getClientOriginalName());
                $Edit->photo = $file->getClientOriginalName();
            }    
       return view('editproduct',compact('Edit','Task1','Task'));
    }
    public function updateproduct(Request $request,$id)
    {
        $Edit=product::find($id);
        $Edit->ProductCategoryID=$request->main;
        $Edit->ProductName=$request->product;
        $Edit->Description=$request->description;
        if(Input::hasFile('image')) {

                $file = Input::file('image');
                $file->move(public_path() . '/photo',$file->getClientOriginalName());
                $Edit->photo = $file->getClientOriginalName();
            }         
     $Edit->isDeleted=$request->delete;
        $Edit->save();
        return redirect ('list');
    }
     public function deleteupdateproduct(Request $request,$id)
    {

        $Edit=Product::find($id);
        $Edit->isDeleted='1';
        $Edit->save();
        return redirect('list');
    }
   
}
