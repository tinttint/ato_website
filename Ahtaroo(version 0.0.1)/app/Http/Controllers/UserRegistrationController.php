<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\customertest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Image;

class UserRegistrationController extends Controller
{
    public function index()
    {  
        $Task = DB::table('customer')->get();
    	return view("customer");
    }
    public function store(Request $request)
    {	
    	$main=new customertest;
    	$main->cname=$request->cname;
    	$main->btype=$request->btype;
    	$main->address=$request->address;
    	$main->city=$request->city;
    	$main->country=$request->country;
    	$main->phone=$request->phone;
    	$main->email=$request->email;
    	$main->website=$request->website;
        $main->logo=$request->logo;
        if (Input::hasFile('logo')){
      $file=Input::file('logo');
      $file->move(public_path().'/logo',$file->getClientOriginalName());
      $main->logo=$file->getClientOriginalName();  
     }
        $main->isDeleted=Input::get('delete');
        $main->save();
        return redirect('customerlist');
        }
    public function showlist()
    {
      $main =DB::table('customer')
            ->select('customer.*','customer.cname','customer.btype','customer.address','customer.city','customer.country','customer.phone','customer.email','customer.website','customer.logo')
            ->get();
      return view("customerlist",compact('main'));
    }
    public function editcustomer($id)
    {
      $Task=DB::table('customer')->get();
      $Edit=customertest::find($id);
      if(Input::hasFile('upload')){

                $file = Input::file('upload');
                $file->move(public_path() . '/uploadfile',$file->getClientOriginalName());
                $Edit->FileName=$file->getClientOriginalName();
            }    
       return view('editcustomer',compact('Edit','Task'));
    }
    public function updatecustomer(Request $request,$id)
    {
        $Edit=customertest::find($id);
        $Edit->cname=$request->cname;
        $Edit->btype=$request->btype;
        $Edit->address=$request->address;
        $Edit->city=$request->city;
        $Edit->country=$request->country;
        $Edit->phone=$request->phone;
        $Edit->email=$request->email;
        $Edit->website=$request->website;
        $Edit->logo=$request->logo;
        if(Input::hasFile('upload')) {

                $file = Input::file('upload');
                $file->move(public_path() . '/uploadfile',$file->getClientOriginalName());
                $Edit->FileName = $file->getClientOriginalName();
            }         
     $Edit->isDeleted=$request->delete;
        $Edit->save();
        return redirect ('customerlist');
    }
     public function deletecustomer(Request $request,$id)
    {

        $Edit=customertest::find($id);
        $Edit->isDeleted='1';
        $Edit->save();
        return redirect('customerlist');
    }
    	
    }
    
