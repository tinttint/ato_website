<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductMainType;
use Illuminate\Support\Facades\DB;
class MainController extends Controller
{
    //
    public function showmaindata()
    {
        $Task=DB::table('productmaintype')->get();
        return view('maintype',compact('Task'));

    }
     public function insertmaintype(Request $request)
   {

    $main=new ProductMainType;
    $main->MainTypeName=$request->main;
    $main->isDeleted=$request->delete;
    $main->save();
    return redirect('maintype');
   }
    public function editmain($id)
    {
        $Edit=ProductMainType::find($id);
         return view('edit',compact('Edit'));

    }

       public function updatemain(Request $request,$id)
    {
        $Edit=ProductMainType::find($id);
        $Edit->MainTypeName=$request->main;
        $Edit->save();
        return redirect ('maintype');
    }
   public function deleteupdatemain(Request $request,$id)
    {

        $Edit=ProductMainType::find($id);
        $Edit->isDeleted='1';
        $Edit->save();
        return redirect('maintype');
    }
}
