<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    //
        //category
      public function showcategorydata()
    {
        $Task1=DB::table('productmaintype')->get();
        $Task = DB::table('category')
            ->join('productmaintype', 'category.MainTypeID', '=', 'productmaintype.id')
            ->select('category.*', 'productmaintype.MainTypeName')
            ->get();
        return view('category',compact('Task','Task1'));

    }
      public function insertcategory(Request $request)
   {

    $main=new Category;
    $main->MainTypeID=$request->main;
    $main->CategoryName=$request->category;
    $main->isDeleted=$request->delete;
    $main->save();
    return redirect('category');
   }
    public function editcategory($id)
    {
      $Task1=DB::table('productmaintype')->get();
      $Edit=Category::find($id);
       return view('editcategory',compact('Edit','Task1'));

    }
       public function updatecategory(Request $request,$id)
    {
        $Edit=Category::find($id);
        $Edit->MainTypeID=$request->main;
        $Edit->CategoryName=$request->category;
        $Edit->save();
        return redirect ('category');
    }
      public function deleteupdatecategory(Request $request,$id)
    {

        $Edit=Category::find($id);
        $Edit->isDeleted='1';
        $Edit->save();
        return redirect('category');
    }
}
