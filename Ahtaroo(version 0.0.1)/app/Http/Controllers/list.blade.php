<section class="white-background" id="contact" style="padding:1%;">
      <div class="container">
        <!-- /row -->
        
        <div class="row" style="padding-top: 10px;">
            <div class="col-md-1"></div>
            <div class="col-md-2">
              <h4 style="color:#5a5a5a;font-family: 'times new roman', sans-serif;font-size: 13px;font-weight: bold;">ACCESS CONTROL SYSTEM</h4>
              <ul style="padding-top: 5px;">
                <li class="a-slog"><a href="{{route('accesscontrol')}}" style="color:#5a5a5a;;font-size:12px;">Finger Print System</a></li>
                <li class="a-slog"><a href="{{route('accesscontrol')}}" style="color:#5a5a5a;font-size:12px;">Face Recognition System</a></li>
                <li class="a-slog"><a href="{{route('accesscontrol')}}" style="color:#5a5a5a;font-size:12px;">Hotel Door Lock System</a></li>
                <li class="a-slog"><a href="{{route('accesscontrol')}}" style="color:#5a5a5a;font-size:12px;">Hotel Safe Box System</a></li>
               <li class="a-slog"><a href="{{route('accesscontrol')}}" style="color:#5a5a5a;font-size:12px;">Gate Control & Parking Management System</a></li>
              </ul>
            </div> 
            <div class="col-md-2" style="padding-left: 2%;">
              <h4 style="color:#5a5a5a;font-family: 'times new roman', sans-serif;font-size: 13px;font-weight: bold;">CCTV SYSTEM</h4>
              <ul style="padding-top: 5px;"><li class="a-slog"><a href="{{route('cctv')}}" style="color:#5a5a5a;font-size: 12px;">  IP CCTV System</a></li>
                <li class="a-slog"><a href="{{route('cctv')}}" style="color:#5a5a5a;font-size: 12px;">  Analoge CCTV System</a></li>
              </ul>
            </div>  
            <div class="col-md-2" style="padding-left: 2%;">
              <h4 style="color:#5a5a5a;font-family: 'times new roman', sans-serif;font-size: 13px;font-weight: bold;">FIRE ALARM SYSTEM</h4>
              <ul>
                <li class="a-slog"><a href="{{route('firealarm')}}" style="color:#5a5a5a;font-size: 12px;">  Addressable Fire Alarm System</a></li>
                <li class="a-slog"><a href="{{route('firealarm')}}" style="color:#5a5a5a;font-size: 12px;">  Conventional Fire ALarm System</a></li>
              </ul>
            </div>
            <div class="col-md-2" style="padding-left: 2%;">
              <h4 style="color:#5a5a5a;font-family: 'times new roman', sans-serif;font-size: 13px;font-weight: bold;">SOFTWARE SOLUTIONS</h4>
              <ul>
                <li  class="a-slog"><a href="{{route('customized')}}" style="color:#5a5a5a;font-size: 12px;">  Hotel PMS Software</a></li>
                <li class="a-slog"><a href="{{route('customized')}}" style="color:#5a5a5a;font-size: 12px;">  Customized Software Solutions</a></li>
                <li class="a-slog"><a href="{{route('customized')}}" style="color:#5a5a5a;font-size: 12px;">  SAP Services & Solutions</a></li>
              </ul>
            </div>
            <div class="col-md-2" style="padding-left: 2%;">
              <h4 style="color:#5a5a5a;font-family: 'times new roman', sans-serif;font-size: 13px;font-weight: bold;">SUPPORT</h4>
              <ul>
              <li class="a-slog"><a href="{{route('cctv')}}" style="color:#5a5a5a;font-size: 12px;"> Project References </a></li>
                
              </ul>
            </div> 
            <div class="col-md-1"></div>
            
            
        </div> <!-- end row -->

      </div>  <!-- container -->

    </section>