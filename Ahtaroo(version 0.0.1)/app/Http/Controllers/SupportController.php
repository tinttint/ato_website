<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Support;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class SupportController extends Controller
{
    //
    public function showsupport()
    {
        $Task1=DB::table('product')->get();
        $Task = DB::table('documenttype')->get();
        return view('support',compact('Task','Task1'));

    }
    public function supportinsert(Request $request)
    {
     $support= new Support;
     $support->ProductID=Input::get('product');
     $support->DocumentTypeID=Input::get('document');
     $support->Code=Input::get('code');
     $support->Description=Input::get('description');
     if (Input::hasFile('upload')){
      $file=Input::file('upload');
      $file->move(public_path().'/uploadfile',$file->getClientOriginalName());
      $support->FileName=$file->getClientOriginalName();  
     }
    $support->isDeleted=Input::get('delete');
     $support->save();
     return redirect('supportlist');
}
public function showlist()
{
     $Task = DB::table('support')
            ->join('product', 'support.ProductID', '=', 'product.id')
            ->join('documenttype','support.DocumentTypeID','=','documenttype.id')
            ->select('support.*', 'product.ProductName','documenttype.documenttype')
            ->get();
    return view('supportlist',compact('Task'));
}
public function editsupport($id)
    {
      $Task1=DB::table('product')->get();
      $Task=DB::table('documenttype')->get();
      $Edit=support::find($id);
      if(Input::hasFile('upload')) {

                $file = Input::file('upload');
                $file->move(public_path() . '/uploadfile',$file->getClientOriginalName());
                $Edit->FileName=$file->getClientOriginalName();
            }    
       return view('editsupport',compact('Edit','Task1','Task'));
    }
    public function updatesupport(Request $request,$id)
    {
        $Edit=support::find($id);
        $Edit->ProductID=$request->product;
        $Edit->Code=$request->code;
        $Edit->DocumentTypeID=$request->document;
        $Edit->Description=$request->description;
        if(Input::hasFile('upload')) {

                $file = Input::file('upload');
                $file->move(public_path() . '/uploadfile',$file->getClientOriginalName());
                $Edit->FileName = $file->getClientOriginalName();
            }         
     $Edit->isDeleted=$request->delete;
        $Edit->save();
        return redirect ('supportlist');
    }
     public function deleteupdatesupport(Request $request,$id)
    {

        $Edit=Support::find($id);
        $Edit->isDeleted='1';
        $Edit->save();
        return redirect('supportlist');
    }
   
}
