<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AhtarooController extends Controller
{
    //
    public function index()
    {
    	return view('index');
    }
    public function customized()
    {
    	return view('customized');
    }
    public function accesscontrol()
    {
    	return view('accesscontrol');
    }
    public function cctv()
    {
    	return view('cctv');
    }
    public function firealarm()
    {
    	return view('firealarm');
    }
    public function pms()
    {
        return view('hotelpms');
    }
    public function contact()
    {
        return view('contact');
    }
    public function sap()
    {
        return view('sap');
    }
    public function about()
    {
        return view('about');
    }
    public function detail()
    {
        return view('softwaredetail');
    }
    
}
