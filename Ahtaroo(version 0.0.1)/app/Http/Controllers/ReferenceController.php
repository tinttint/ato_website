<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class ReferenceController extends Controller
{
    //
    public function index(){
    	$Task = DB::table('productcustomer')
            ->join('customer', 'productcustomer.CustomerID', '=', 'customer.id')
            ->select('customer.*','productcustomer.ProductID')
            ->orderby('cname','asc')
            ->get();
        return view('reference',compact('Task'));
    }
    
}
