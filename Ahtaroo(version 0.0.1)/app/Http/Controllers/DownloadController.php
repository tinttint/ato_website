<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Support;
use App\registertest;
use Input;
class DownloadController extends Controller
{
	public function index(Request $request)
    {
         $Task = DB::table('support')
            ->join('product', 'support.ProductID', '=', 'product.id')
            ->join('documenttype', 'support.DocumentTypeID', '=', 'documenttype.id')
            ->select('support.*','product.ProductName')
            ->get();
        return view('usersupport',compact('Task'));
    }
     public function store(Request $request){
        $main=new registertest;
        $main->name=$request->name;
        $main->email=$request->email;
        $main->org=$request->org;
        $main->phone=$request->phone;
        $main->support_id=$request->id;
        $main->save();
        return redirect('downloadpage');
    }

    public function download(Request $request)
    {
        $id = $request->id;
        $data = DB::table('register')
                ->join('support','support.id','=','register.support_id')
                ->first();
        $file_path = public_path('uploadfile').'/'.$data->FileName;
        return response()->download($file_path);
    }
    public function showlist()
    {
        $data = DB::table('register')->get();
        return view('download', ['data' => $data]);
    }
    public function detail()
    {
        return view('softwaredetail');
    }
    
}