<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCustomerModel extends Model
{
    //
    protected $table='productcustomer';
    protected $fillable=['ProductID','CustomerID','isDeleted'];
}
