<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMainType extends Model
{
    //
    protected $table = 'ProductMainType';
    protected $fillable = ['MainTypeName','isDeleted'];
   

}
