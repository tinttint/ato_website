<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customertest extends Model
{
	protected $table='customer';
    protected $fillable=['cname','btype','address','city','country','phone','email','website','logo','isDeleted'];
}
