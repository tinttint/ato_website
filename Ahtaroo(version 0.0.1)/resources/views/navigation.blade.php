<style> 
   .navbar {
      margin-bottom: 0;
      background-color: #fff;
      z-index: 9999;
      border: 0;
      line-height: 1.42857143 !important;
      letter-spacing: 1px;
      border-radius: 0;
      font-family: 'times new roman', sans-serif;
      height: auto;  
  }
  .navbar li a, .navbar .navbar-brand {
      color:#000 !important;
      font-size: 14px !important;
      font-weight: bold;
      margin-top: 0px;
      font-family: 'OpenSans-Bold',sans-serif;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
      color: #a62a2a !important;
      border-bottom:1px red solid;
  }
  .navbar-default .navbar-toggle {
      border-color: transparent;
      color: #fff !important;
  }
  .navbar-header{
    height: auto;
  }
   .contact{
    border:1px;
    border-style: solid;
    border-color: #a62a2a;
    border-radius: 2px;
    padding:0.5% 1% 0.5% 1%;
    float: right;
    color: #a62a2a !important;
    margin:1% 1.8% 1% 1.8%;
    font-family: 'OpenSans-Bold',sans-serif;
    font-size: 14px;
  }
  .contact:hover{
    border-radius: 5px;
    color:#fff;
    background-color: #000;
  }

</style>
 <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                       
      </button>
      <img src="{{asset('images/logo.png')}}" class="img-responsive" style="width: 50px;height:45px; margin-left: 0; margin-top:6px;">
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
    
<a href="contact" class="contact">Contact us</a>
      <ul class="nav navbar-nav navbar-right">
            <li><a href="{{route('home')}}" class="page-scroll">Home</a></li>
            <li><a href="about" class="page-scroll">About</a></li>
            <li><a href="{{route('home')}}#two" class="page-scroll">Software</a></li>
            <li><a href="{{route('home')}}#three" class="page-scroll">Product</a></li>
            <li><a href="download" class="page-scroll">Support</a></li>
      </ul>
      
    </div>
  </div>
</nav>
<!--end of nav!-->