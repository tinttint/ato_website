
@extends('layout')
<style>
.panel-heading{
        font-weight: bold;
        font-size: 20px;
        }
</style>
@section('content')<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Product Category Type</div>
                <div class="panel-body">
                <div class="panel-body">
                    <form class="form-horizontal main" role="form" method="POST" action="updatecategory&<?php echo $Edit->id?>">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Product Main Type Name</label>
                          <div class="col-md-6">
                        <select name="main" class="form-control">
                          <?php

                               foreach ($Task1 as $value){
                                        $delete=$value->isDeleted;
                                        if($delete=='0')
                                        {                           
                                      
                        ?>
                               
                               <option value="<?php echo $value->id?>"><?php echo $value->MainTypeName?></option>
                           
                               
                               <?php
                             }
                      }
                      
                            ?>
                            </select>
                            
                            </div>
                         </div>
                        <div class="form-group">
                            <label for="task" class="col-md-4 control-label">Category Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="category" value="{{$Edit->CategoryName}}">
                            </div>
                        </div>

                        <input type="hidden" name="delete" value="0">
                                       <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Edit</button>
                                <button type="reset" class="btn btn-primary">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection