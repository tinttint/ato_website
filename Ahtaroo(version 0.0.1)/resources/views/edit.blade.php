@extends('layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Task</div>
                <div class="panel-body">
                    <form class="form-horizontal edit" role="form" method="POST" action="update&<?php echo $Edit->id?>">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="task" class="col-md-4 control-label">Product Main Type Name</label>

                            <div class="col-md-6">
                                <input id="task" type="text" class="form-control" name="main" value="{{$Edit->MainTypeName}}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="reset" class="btn btn-primary">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection