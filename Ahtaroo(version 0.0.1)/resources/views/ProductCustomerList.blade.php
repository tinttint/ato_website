@extends('layout')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="bootstrap.min.css">

<style>
    .edit,.delete{
    background-color: #180aa3;
    color: white;
    padding: 5px 10px;
    border-radius: 25px;
    text-align: center; 
    text-decoration: none;
    display: inline-block;
    }
    .panel-heading{
        font-weight: bold;
        font-size: 20px;
        }
</style>
@section('content')
<div class="container" style="margin-top: -50px; padding: 0px 70px 0 70px;">
    <h3 style="color: #9f1d28;">Product Customer List</h3> 

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead style="background-color: #9f1d28; color:#fff;" >
                                    <tr>
                                        <th>No</th>
                                        <th>Product</th>
                                        <th>Customer</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                            
                               <?php
                                $i=1;

                                     foreach ($Task as $value) {
                                      $delete=$value->isDeleted;
                                     
                                      if($delete=='0')
                                      {
                                                                           
                                    ?>
                                <tr>
                                <td style="width:10%;"><?php echo $i ?></td>
                                <td style="width: 20%;"><?php echo $value->ProductName;?></td>
                                <td style="width: 20%;"><?php echo $value->cname;?></td>
                                <td style="width: 20%; font-size: 14px;"><a href="editlist&<?php echo $value->id?>" class="edit">Edit</a>
                                   <a href="deletelist&<?php echo $value->id?>" class="delete">Delete</a>
                                </td>
                                </tr>
                                    
                                      
                                <?php
                                $i++;
                                 }
                                }
                           
                                ?>

                               </tbody>
                                </table>
                                </div>
                                </div>
@endsection