<!DOCTYPE html>
<!--
++++++++++++++++++++++++++++++++++++++++++++++++++++++
AUTHOR : Vijayan PP
PROJECT :NIM
VERSION : 1.1
++++++++++++++++++++++++++++++++++++++++++++++++++++++
-->
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ahtar Oo Co.,Ltd</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- [ FONT-AWESOME ICON ] 
        =========================================================================================================================-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- [ PLUGIN STYLESHEET ]
        =========================================================================================================================-->
  <link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
  <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
  <!-- [ Boot STYLESHEET ]
        =========================================================================================================================-->
  
        <!-- [ DEFAULT STYLESHEET ] 
        =========================================================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/color/rose.css')}}"> 
</head>
<style>

    .hr >.container
{
    padding: 6% 6% 1% 6%;
}

.hr i.fa {
  float: left;
  font-size:1.6em;
  width: 70px;
        height:70px;
        background:#fff;
        border-radius:50%;
        color:#000; 
        text-align: center;
        padding-top:25px;
        transition:all 200ms ease-in;
        -webkit-transition:all 200ms ease-in;
        -moz-transition:all 200ms ease-in;
        -ms-transition:all 200ms ease-in;
}
.hr .hr-detail {
  margin-left:90px;
}
.hr .hr-detail h4 {
font-family: 'Roboto Slab', serif;
}
.hr .hr-detail p {
  color: #fff;
        margin-top:10px;
        font-size:0.83em;
        color:#d9d9d9;
}

.hr:hover>i.fa
{
    background:#800000;
    color:#fff;
    box-shadow:2px 3px 5px 8px rgba(0,0,0,0.3);
    -webkit-box-shadow:2px 3px 5px 2px rgba(0,0,0,0.3);
    -moz-box-shadow:2px 3px 5px 2px rgba(0,0,0,0.3);
    -ms-box-shadow:2px 3px 5px 2px rgba(0,0,0,0.3);
}
.hr{
  background-color: #800000;
}
.nav a{
  font-family: 'times new roman', sans-serif;
  font-size: 16px;
}
.aboutus{
  padding-top:6%;
  background-color: #800000;
}

a{
    font-size: 14px;
    font-family:'OpenSans-Bold',sans-serif;
    color: #5a5a5a;
}
.software{
  padding-top:10%;
}
a:hover{
  color:#484848;

}
  .footer{
    background-color: #fff;
    padding-top: 2%;
  }
  address{
    font-family: 'mmrtext', sans-serif;
    font-size: 16px;
    color: #800000;
  }
  small{
    color: #800000;
    margin-left:45%;
  }

p{
  font-family: 'mmrtext',sans-serif;
  font-size: 14px;
  text-align: justify;
  padding-top:20px;
}

</style>
<body >
<!-- [ LOADERs ]
================================================================================================================================--> 
    <div class="preloader">
    <div class="loader theme_background_color">
        <span></span>
      
    </div>
</div>
<!-- [ /PRELOADER ]
=============================================================================================================================-->
<!-- [WRAPPER ]
=============================================================================================================================-->
<div class="wrapper">
  <!-- [NAV]
 ============================================================================================================================-->    
   <!-- Navigation
    ==========================================-->
    @include('navigation')
         
    <img src="{{asset('images/up.png')}}" style="width: 50px; height: 50px;" onclick="topFunction()" id="myBtn" title="Go to top">
    <script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>

<section class="white" id="contact" style="padding:1%;background-color: #fff;">
<div class="container" style="padding: 5% 3% 12% 3%">
    <div class="row" style="padding-top: 1%;">
    <div class="col-sm-4 col-md-3 sidebar">
        <div>
        <ul>
        <li class="software"><a href="#" class="toggleButton" data-target="div2">
          HR Management System
        </a></li>
        <li class="software"><a href="#" class="toggleButton" data-target="div3">
            Performance Management System
        </a></li>
        <li class="software"><a href="#" class="toggleButton" data-target="div4">
             Issue Ticketing System
        </a></li>
       <li class="software"><a href="#" class="toggleButton" data-target="div5">
            Cardiac Surgery Recore Management System
        </a></li>
       <li class="software"> <a href="#" class="toggleButton" data-target="div6">
            Attendance Management System
        </a></li>
        <li class="software"> <a href="#" class="toggleButton" data-target="div7">
            Employee Performance Review System
        </a></li>
    </div> 
         
</div>
<div class="col-sm-1 col"></div>
<div class="col-sm-7 col-md-8 black">
<div id="div2" class="toggleDiv" style="display:none">
<div class="row" style="padding-top: 20px;">
  <h3>HR Management System</h3>
  <div class="col-md-12">
    <p>HRMS helps management and HR personals to manage and keep track of the appli.HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Recruitment Management</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Employee Information Management</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS ability to hold information on different areas of Employee in a central system helps Management and HR personals accurately record all Employee data and manage them effectively. Centralized data and a variety of reports also make tracking of Employee’s progress easier.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;" >
  <div class="col-md-12">
  <h3>Attendance Management</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>With HRMS, monitoring of employees’ in-time, out-time, total number of hours worked including overtimes and shift schedules can be done easily. Daily, Weekly and Monthly attendance reports and custom reports for individual employees, departments and location enable management to track employees’ attendance anytime. The system can also be integrated with Attendance Terminals such as Finger Print terminals and Face Recognition terminals.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Leave Management</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>All types of leaves across your organization can be recorded and monitored through analytic reports such as leave balances reports, leave applications reports etc.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Training Management</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to plan and keep track of training programs and employee progress to maintain the development skills for job productivity.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Requisition Management</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to record the requisition information such as OT and Allowances.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Payroll Management</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module provides to manage all the payroll information and easy payroll calculation including salary settings, deductions, allowances, taxes.</p>
    </div>
  </div>
</div>
</div>


<div id="div4" class="toggleDiv" style="display:none">
<div class="row" style="padding-top: 20px;">
  <h3>Issue Ticket System</h3>
  <div class="col-md-12">
    <p>HRMS helps management and HR personals to manage and keep track of the appli.HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Ticket Entry</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Ticket Assigning</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS’ ability to hold information on different areas of Employee in a central system helps Management and HR personals accurately record all Employee data and manage them effectively. Centralized data and a variety of reports also make tracking of Employee’s progress easier.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;" >
  <div class="col-md-12">
  <h3>Attendance Management System</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>With HRMS, monitoring of employees’ in-time, out-time, total number of hours worked including overtimes and shift schedules can be done easily. Daily, Weekly and Monthly attendance reports and custom reports for individual employees, departments and location enable management to track employees’ attendance anytime. The system can also be integrated with Attendance Terminals such as Finger Print terminals and Face Recognition terminals.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Solving & Ticket Closing</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>All types of leaves across your organization can be recorded and monitored through analytic reports such as leave balances reports, leave applications reports etc.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Ticket Transferring within Groups</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to plan and keep track of training programs and employee progress to maintain the development skills for job productivity.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Main ticket & Sub tickets Opening</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to record the requisition information such as OT and Allowances.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Multi User grouping</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module provides to manage all the payroll information and easy payroll calculation including salary settings, deductions, allowances, taxes.</p>
    </div>
  </div>
</div>
</div>


<div id="div3" class="toggleDiv" style="display:none">
<div class="row" style="padding-top: 20px;">
  <h3>Performance Management System</h3>
  <div class="col-md-12">
    <p>HRMS helps management and HR personals to manage and keep track of the appli.HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Requirement Management System</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Employee Information Management System</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS’ ability to hold information on different areas of Employee in a central system helps Management and HR personals accurately record all Employee data and manage them effectively. Centralized data and a variety of reports also make tracking of Employee’s progress easier.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;" >
  <div class="col-md-12">
  <h3>Attendance Management System</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>With HRMS, monitoring of employees’ in-time, out-time, total number of hours worked including overtimes and shift schedules can be done easily. Daily, Weekly and Monthly attendance reports and custom reports for individual employees, departments and location enable management to track employees’ attendance anytime. The system can also be integrated with Attendance Terminals such as Finger Print terminals and Face Recognition terminals.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Leave Management System</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>All types of leaves across your organization can be recorded and monitored through analytic reports such as leave balances reports, leave applications reports etc.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Training Management System</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to plan and keep track of training programs and employee progress to maintain the development skills for job productivity.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Requisition Management System</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to record the requisition information such as OT and Allowances.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Payroll Management System</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module provides to manage all the payroll information and easy payroll calculation including salary settings, deductions, allowances, taxes.</p>
    </div>
  </div>
</div>
</div>



<div id="div5" class="toggleDiv" style="display:none">
<div class="row" style="padding-top: 20px;">
  <h3>Cardiac Surgery Record Management System</h3>
  <div class="col-md-12">
    <p>A web based Patient’s record management system developed for Yankin Children Hospital Cardiac Surgery Department that enables medical professionals to access the patient’s surgery records and medical records from Admission till discharge anytime and anywhere. 
The system supports knowledge bank for medical professionals with information such as medical examinations, patient’s demographics, diagnoses & treatments, operation results for both child surgery and adult surgery that are important for patient’s care. 
The system supports the fast and accurate data and variety of reports associate with Cardiac Surgery and are important for research and development of medical industry.</p>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Patient Demographic</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Hospital Admission Entry</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS’ ability to hold information on different areas of Employee in a central system helps Management and HR personals accurately record all Employee data and manage them effectively. Centralized data and a variety of reports also make tracking of Employee’s progress easier.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;" >
  <div class="col-md-12">
  <h3>Pre-operative Record Entry</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>With HRMS, monitoring of employees’ in-time, out-time, total number of hours worked including overtimes and shift schedules can be done easily. Daily, Weekly and Monthly attendance reports and custom reports for individual employees, departments and location enable management to track employees’ attendance anytime. The system can also be integrated with Attendance Terminals such as Finger Print terminals and Face Recognition terminals.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Operation Status Record</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>All types of leaves across your organization can be recorded and monitored through analytic reports such as leave balances reports, leave applications reports etc.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Surgery Types</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to plan and keep track of training programs and employee progress to maintain the development skills for job productivity.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Post-operative Record Entry</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to record the requisition information such as OT and Allowances.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Operation Reports</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module provides to manage all the payroll information and easy payroll calculation including salary settings, deductions, allowances, taxes.</p>
    </div>
  </div>
</div>
</div>


<div id="div6" class="toggleDiv" style="display:none">
<div class="row" style="padding-top: 20px;">
  <h3>Attendance Management System</h3>
  <div class="col-md-12">
    <p>The system provide monitoring of employees’ in-time, out-time, total number of hours worked including overtime and shift schedules can be done easily.
Daily, Weekly and Monthly attendance reports and custom reports for individual employees, departments and location enable management to track employees’ attendance anytime.</p>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Integration with Attendance Terminals (Such as Fingerprint, Face Recognition Device)</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Shift Scheduling</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS’ ability to hold information on different areas of Employee in a central system helps Management and HR personals accurately record all Employee data and manage them effectively. Centralized data and a variety of reports also make tracking of Employee’s progress easier.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;" >
  <div class="col-md-12">
  <h3>Attendance Data download & import</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>With HRMS, monitoring of employees’ in-time, out-time, total number of hours worked including overtimes and shift schedules can be done easily. Daily, Weekly and Monthly attendance reports and custom reports for individual employees, departments and location enable management to track employees’ attendance anytime. The system can also be integrated with Attendance Terminals such as Finger Print terminals and Face Recognition terminals.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Attendance Calculation</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>All types of leaves across your organization can be recorded and monitored through analytic reports such as leave balances reports, leave applications reports etc.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Daily Weekly and Monthly Reports ( Individual, Departments, Locations )</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to plan and keep track of training programs and employee progress to maintain the development skills for job productivity.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Leave Management</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>All types of leaves across your organization can be recorded and monitored through analytic reports such as leave balances reports, leave applications reports etc.</p>
    </div>
  </div>
</div>
</div>


<div id="div7" class="toggleDiv" style="display:none">
<div class="row" style="padding-top: 20px;">
  <h3>Employee Performance Review System</h3>
  <div class="col-md-12">
    <p>HRMS helps management and HR personals to manage and keep track of the appli.HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Ticket Entry</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Ticket Assigning</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>HRMS’ ability to hold information on different areas of Employee in a central system helps Management and HR personals accurately record all Employee data and manage them effectively. Centralized data and a variety of reports also make tracking of Employee’s progress easier.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;" >
  <div class="col-md-12">
  <h3>Attendance Management System</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>With HRMS, monitoring of employees’ in-time, out-time, total number of hours worked including overtimes and shift schedules can be done easily. Daily, Weekly and Monthly attendance reports and custom reports for individual employees, departments and location enable management to track employees’ attendance anytime. The system can also be integrated with Attendance Terminals such as Finger Print terminals and Face Recognition terminals.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Solving & Ticket Closing</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>All types of leaves across your organization can be recorded and monitored through analytic reports such as leave balances reports, leave applications reports etc.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Ticket Transferring within Groups</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to plan and keep track of training programs and employee progress to maintain the development skills for job productivity.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Main ticket & Sub tickets Opening</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module manages to record the requisition information such as OT and Allowances.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <h3>Multi User grouping</h3>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p>This module provides to manage all the payroll information and easy payroll calculation including salary settings, deductions, allowances, taxes.</p>
    </div>
  </div>
</div>
</div>



  </div>
  </div>
  </div>
  <hr>
  </section>

  

<!-- /access control system
=====================================================================================================-->

 
 <!-- [/Hotel Door Lock]
 ============================================================================================================================-->

 
 <!-- [/SERVICES]
 ============================================================================================================================-->
 
 
  
 
@include('list')
 
 <!-- [/FOOTER]
 ============================================================================================================================-->
 
 
 
</div>
 <script>
  $(function(){

   $('.toggleButton').click(function(){

         var target = $('#' + $(this).attr('data-target'));
         $('.toggleDiv').not(target).hide();
         target.show();
   });
});
 </script>

<!-- [ /WRAPPER ]
=============================================================================================================================-->

  <!-- [ DEFAULT SCRIPT ] -->
 <script src="{{asset('library/modernizr.custom.97074.js')}}"></script>
      <script src="{{asset('library/jquery-1.11.3.min.js')}}"></script>
        <script src="{{asset('library/bootstrap/js/bootstrap.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>  
  <!-- [ PLUGIN SCRIPT ] -->
        <script src="{{asset('library/vegas/vegas.min.js')}}"></script>
  <script src="{{asset('js/plugins.js')}}"></script>
        <!-- [ TYPING SCRIPT ] -->
         <script src="{{asset('js/typed.js')}}"></script>
         <!-- [ COUNT SCRIPT ] -->
         <script src="{{asset('js/fappear.js')}}"></script>
       <script src="{{asset('js/jquery.countTo.js')}}"></script>
  <!-- [ SLIDER SCRIPT ] -->  
        <script src="{{asset('js/owl.carousel.js')}}"></script>
         <script src="{{asset('js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript" src="{{asset('js/SmoothScroll.js')}}"></script>
        
        <!-- [ COMMON SCRIPT ] -->
  <script src="{{asset('js/common.js')}}"></script>


  
</body>


</html>