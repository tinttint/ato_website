@extends('layout')
<style>
    .edit,.delete{
    background-color: #180aa3;
    color: white;
    padding: 5px 10px;
    border-radius: 25px;
    text-align: center; 
    text-decoration: none;
    display: inline-block;
    }
    .panel-heading{
        font-weight: bold;
        font-size: 20px;
        }
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Product Customer</div>
                <div class="panel-body">
                    <form class="form-horizontal main" role="form" method="POST" action="insert_productcustomer" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="product" class="col-md-4 control-label">Product</label>
                         <div class="col-md-6">
                          <select name="product" class="form-control">
                          <?php
                                    foreach ($Task1 as $value){
                                        $delete=$value->isDeleted;
                                     
                                      if($delete=='0')
                                      {
                          ?>
                               <option value="<?php echo $value->id;?>"><?php echo $value->ProductName?></option>
                               
                               <?php
                            }
                        }
                            ?>
                            </select>
                            
                            </div>
                         </div>
                         
                        <div class="form-group">
                            <label for="task" class="col-md-4 control-label">Customer</label>

                            <div class="col-md-6">
                                <select name="customer" class="form-control">
                          <?php
                                    foreach ($Task as $value){
                                      $delete=$value->isDeleted;

                                      if($delete=='0')
                                      {
                                       
                               ?>
                               <option value="<?php echo $value->id;?>"><?php echo $value->cname?></option>
                               
                               <?php
                               
                           }
                         }
                        
                            ?>
                            </select>
                            
                            </div>
                        </div>
                        <input type="hidden" name="delete" value="0">
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-primary">Cancel</button>
                            </div>
                        </div>
                                      
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
     
                           
@endsection