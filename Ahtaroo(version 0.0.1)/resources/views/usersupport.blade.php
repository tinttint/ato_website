<!DOCTYPE html>
<!--
++++++++++++++++++++++++++++++++++++++++++++++++++++++
AUTHOR : Vijayan PP
PROJECT :NIM
VERSION : 1.1
++++++++++++++++++++++++++++++++++++++++++++++++++++++
-->
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ahtar Oo Co.,Ltd</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <link href="fonts/Open_Sans/OpenSans-Bold" rel="stylesheet">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <!-- [ FONT-AWESOME ICON ] 
        =========================================================================================================================-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- [ PLUGIN STYLESHEET ]
        =========================================================================================================================-->
  <link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
  <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
  <!-- [ Boot STYLESHEET ]
        =========================================================================================================================-->
  
        <!-- [ DEFAULT STYLESHEET ] 
        =========================================================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('fonts/Open_Sans/OpenSans-Bold')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/color/rose.css')}}"> 
</head>
<style>

.aboutus{
  padding-top:6%;
}
.aboutus{
  background-color: #800000;
}

a{
    font-size: 12px;
    font-family:'OpenSans-Bold',sans-serif;
    font-weight: bold;
}
.box {
  width: 40%;
  margin: 0 auto;
  background: rgba(255,255,255,0.2);
  padding: 35px;
  border: 2px solid #fff;
  border-radius: 20px;
  background-clip: padding-box;
  text-align: center;
}

.button {
  font-size: 1em;
  padding: 10px;
  color: #fff;
  border: 2px solid #06D85F;
  border-radius: 20px/50px;
  text-decoration: none;
  cursor: pointer;
  transition: all 0.3s ease-out;
}
.button:hover {
  background: #06D85F;
}

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 50px;
  right: 50px;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 70px auto;
  padding: 20px;
  background: #e6e6e6;
  border-radius: 5px;
  width: 30%;
  position: relative;
  transition: all 5s ease-in-out;
}

.popup h2 {
  margin-top: 0;
  color: #000;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #800000;
}
.popup .content {
  max-height: 30%;
  overflow: auto;

}

@media screen and (max-width: 700px){
  .box{
    width: 70%;
  }
  .popup{
    width: 70%;
  }
}.a9 {margin-top:300px; margin-left:300px; float:left;}

div>.col-md-7{
  border-radius: 10px;
}
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    opacity: 0;
    color:#5a5a5a;

}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 2px solid #888;
    width: 80%;


}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
    position: absolute;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>
<body >
<!-- [ LOADERs ]
================================================================================================================================--> 
    
<!-- [ /PRELOADER ]
=============================================================================================================================-->
<!-- [WRAPPER ]
=============================================================================================================================-->
<div class="wrapper">
  <!-- [NAV]
 ============================================================================================================================-->    
   <!-- Navigation
    ==========================================-->
    @include('navigation')
         
    <img src="{{asset('images/up.png')}}" style="width: 50px; height: 50px;" onclick="topFunction()" id="myBtn" title="Go to top">
    <script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>
   <!-- [/NAV]
 ============================================================================================================================--> 
 <section class="aboutus" id="software">
     <div class="container">
         <div class="row">
             <div class="col-md-12 text-center black">
                 <h3 class="title white">Support</h3>
            <p class="white" style="text-align: center; font-size: 14px;max-width: 700px;margin:0 auto;padding-top:10px;">{{trans('customized.about')}}</p> 
             </div>
         </div>
     </div>
 </section>
 <!-- [access control system]
 ============================================================================================================================-->
<section class="white" id="contact" style="background-color: #fff;">
<div class="container" style="padding: 5% 3% 12% 3%">
    <div class="row" style="padding-top: 1%;">
    <div class="col-sm-4 col-md-3 sidebar">
    <div class="mini-submenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </div>
    <div class="list-group">
        <a href="#finger" class="list-group-item toggleButton" data-target="div2">
            <i class="font fa fa-angle-double-right text-primary"></i> Brochures
        </a>
        <a href="#" class="list-group-item toggleButton" data-target="div3">
            <i class="font fa fa-angle-double-right text-primary"></i> Data Sheet
        </a>
        <a href="#" class="list-group-item toggleButton" data-target="div4">
            <i class="font fa fa-angle-double-right text-primary"></i> User Manual
        </a>
        <a href="#" class="list-group-item toggleButton" data-target="div5">
            <i class="font fa fa-angle-double-right text-primary"></i>SDK
        </a>
    </div> 
   
        
        
           
</div>
<div class="col-sm-8 col-md-9 black" >

<div id="div2" class="toggleDiv" style="display:none">
<h2 style="font-family: 'OpenSans-Bold',sans-serif; color: #5a5a5a; padding-bottom: 1.5%;font-weight: bold;">Brochures</h2><hr style="color:#f2f2f2; border:2px solid">
   <?php
        foreach ($Task as $value){
           $delete=$value->isDeleted;
                 $document=$value->DocumentTypeID;
             if($delete=='0')
             {
                if($document=='1')
                {
                        ?>
                
<div class="row" style="padding-bottom: 1%;">
<div class="col-md-12">
   
   <p style="color: #5a5a5a;font-family: 'Open_Sans',sans-serif;font-size: 14px;padding-top: 1%;"><?php echo $value->FileName?><a href="#myModal" class="announce"; style="float: right;" data-toggle="modal" data-id="<?php echo $value->id?>" data-target="#myModal" data-backdrop="false"><img src="{{asset('images/download.jpg')}}"></a>
    </p>

</div>
</div><hr style="margin-bottom:0; border-style: dotted;">
<?php
                            }
                        }
                    }
                            ?>
  </div>

<div id="div3" class="toggleDiv" style="display:none">
    <h2 style="font-family: 'OpenSans-Bold',sans-serif; color: #5a5a5a; padding-bottom: 1.5%;font-weight: bold;">Data Sheet</h2><hr style="color:#f2f2f2; border:2px solid">
   <?php
        foreach ($Task as $value){
           $delete=$value->isDeleted;
                 $document=$value->DocumentTypeID;      
             if($delete=='0')
             {
                if($document=='2')
                {
                        ?>
                
<div class="row" style="padding-bottom: 1%;">
<div class="col-md-12">
   
   <p style="color: #5a5a5a;font-family: 'Open_Sans',sans-serif;font-size: 14px;padding-top: 1%;",><?php echo $value->FileName?><a href="#myModal" style="float: right;"><img src="{{asset('images/download.jpg')}}"></a>
    </p>

</div>
</div><hr style="margin-bottom:0; border-style: dotted;">
<?php
                            }
                        }
                    }
                            ?>
  </div>

  <div id="div4" class="toggleDiv" style="display:none">
    <h2 style="font-family: 'OpenSans-Bold',sans-serif; color: #5a5a5a; padding-bottom: 1.5%;font-weight: bold;">User Manual</h2><hr style="color:#f2f2f2; border:2px solid">
   <?php
        foreach ($Task as $value){
           $delete=$value->isDeleted;
                 $document=$value->DocumentTypeID;      
             if($delete=='0')
             {
                if($document=='3')
                {
                        ?>
                
<div class="row" style="padding-bottom: 1%;">
<div class="col-md-12">
   
   <p style="color: #5a5a5a;font-family: 'Open_Sans',sans-serif;font-size: 14px;padding-top: 1%;",><?php echo $value->FileName?><a href="#myModal" style="float: right;"><img src="{{asset('images/download.jpg')}}"></a>
    </p>

</div>
</div><hr style="margin-bottom:0; border-style: dotted;">
<?php
                            }
                        }
                    }
                            ?>
  </div>

   <div id="div5" class="toggleDiv" style="display:none">
    <h2 style="font-family: 'OpenSans-Bold',sans-serif; color: #5a5a5a; padding-bottom: 1.5%;font-weight: bold;">SDK</h2><hr style="color:#f2f2f2; border:2px solid">
   <?php
        foreach ($Task as $value){
           $delete=$value->isDeleted;
                 $document=$value->DocumentTypeID;      
             if($delete=='0')
             {
                if($document=='4')
                {
                        ?>
                
<div class="row" style="padding-bottom: 1%;">
<div class="col-md-12">
   
   <p style="color: #5a5a5a;font-family: 'Open_Sans',sans-serif;font-size: 14px;padding-top: 1%;",><?php echo $value->FileName?><a href=""style="float: right;"><img src="{{asset('images/download.jpg')}}"></a>
    </p>

</div>
</div><hr style="margin-bottom:0; border-style: dotted;">
<?php
                            }
                        }
                    }
                            ?>
  </div>

  </div>
  </div>
  </div>
  </section> 

  <form class="form-horizontal main" role="form" method="POST" action="register_customer" enctype="multipart/form-data">
                        {{ csrf_field() }}  

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" style="padding-top:60px;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="padding-top: 20px;">
        
    <div class="row">
      <div class="col-md-10"><h3 style="text-align: center;font-family: 'OpenSans-Bold',sans-serif; ">Please Fill Your Details</h3></div>
       <div class="col-md-2"><button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">x</span></button></div><br><br>
      
    </div>
    
    
    <div class="row">
      <div class="col-md-5">
        <label style="text-align: center;font-family: 'OpenSans-Bold',sans-serif;">Full Name</label></div>
        <div class="col-md-7">
          <input type="text" name="name" id="name" style="border-radius: 3px;" /><br><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
<label style="text-align: center;font-family: 'OpenSans-Bold',sans-serif;">
        Email</label></div>
        <div class="col-md-7">
          <input type="email" value="" name="email" id="email" style="border-radius: 3px;"/><br><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
      <label style="text-align: center;font-family: 'OpenSans-Bold',sans-serif;">
       Organization</label></div>
       <div class="col-md-7">
        <input type="text" name="org" id="org" style="border-radius: 3px;"/><br><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
      <label style="text-align: center;font-family: 'OpenSans-Bold',sans-serif;">
        Phone Number</label></div>
        <div class="col-md-7">
          <input type="text" value="" name="phone" id="phone" style="border-radius: 3px;"/><br><br>
          <input type="hidden" name="id" id="id">
      </div>
    </div>
    <div class="row">
        
        <div class="col-md-12 text-center">
          <input type="submit" value="Login" class="btn btn-primary" id="hide" style="border-radius: 7px;">

       </div>
    </div>
        
  </div>

      </div>
      
    </div>
  </div>
  
</form>
 
@include('list')
   @include('address')
 
 <!-- [/FOOTER]
 ============================================================================================================================-->
 
 
 
</div>
 <script>
  $(function(){

   $('.toggleButton').click(function(){

         var target = $('#' + $(this).attr('data-target'));
         $('.toggleDiv').not(target).hide();
         target.show();
   });
});
  $(document).ready(function(){
   $(".announce").click(function(){ // Click to only happen on announce links
     $("#id").val($(this).data('id'));
   });
});
 </script>

<!-- [ /WRAPPER ]
=============================================================================================================================-->

  <!-- [ DEFAULT SCRIPT ] -->
 <script src="{{asset('library/modernizr.custom.97074.js')}}"></script>
      <script src="{{asset('library/jquery-1.11.3.min.js')}}"></script>
        <script src="{{asset('library/bootstrap/js/bootstrap.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>  
  <!-- [ PLUGIN SCRIPT ] -->
        <script src="{{asset('library/vegas/vegas.min.js')}}"></script>
  <script src="{{asset('js/plugins.js')}}"></script>
        <!-- [ TYPING SCRIPT ] -->
         <script src="{{asset('js/typed.js')}}"></script>
         <!-- [ COUNT SCRIPT ] -->
         <script src="{{asset('js/fappear.js')}}"></script>
       <script src="{{asset('js/jquery.countTo.js')}}"></script>
  <!-- [ SLIDER SCRIPT ] -->  
        <script src="{{asset('js/owl.carousel.js')}}"></script>
         <script src="{{asset('js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript" src="{{asset('js/SmoothScroll.js')}}"></script>
        
        <!-- [ COMMON SCRIPT ] -->
  <script src="{{asset('js/common.js')}}"></script>


  
</body>


</html>