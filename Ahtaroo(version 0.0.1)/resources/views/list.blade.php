   <link href="fonts/Open_Sans/OpenSans-Bold" rel="stylesheet"> 
<style>
  .circle{
  border-radius: 60%;
  margin: 0.15em;
  font-size: 1.3em;
  }
  
.fa-twitter{
  background: #5c5c5c;
  color: #FFFFFF;
  padding: 0.08em 0.07em;
  }
  .fa-facebook{
  background: #5c5c5c;
  color: #FFFFFF;
  padding: 0.08em 0.3em;
  margin-left:10%;
  margin-top: 2%;
  }
</style>
<section class="white-background">
      <div class="container" style="padding-bottom: 4%;padding-top: 4%;">
        <!-- /row -->
        
        <div class="row" style="padding-left: 10%;padding-right: 10%;">
            <div class="col-md-3">
              <h4 style="color:#5a5a5a;font-family: 'OpenSans-Bold',sans-serif; font-size: 13px;font-weight: bold;">ACCESS CONTROL SYSTEM</h4>
              <ul style="padding-top:3%;">
                <li class="a-slog"><a href="{{route('accesscontrol')}}" style="color:#5a5a5a;;font-size:12px;">Finger Print System</a></li>
                <li class="a-slog"><a href="{{route('accesscontrol')}}" style="color:#5a5a5a;font-size:12px;">Face Recognition System</a></li>
                <li class="a-slog"><a href="{{route('accesscontrol')}}" style="color:#5a5a5a;font-size:12px;">Hotel Door Lock System</a></li>
                <li class="a-slog"><a href="{{route('accesscontrol')}}" style="color:#5a5a5a;font-size:12px;">Hotel Safe Box System</a></li>
               <li class="a-slog"><a href="{{route('accesscontrol')}}" style="color:#5a5a5a;font-size:12px;">Gate Control & Parking Management System</a></li>
              </ul>
            </div> 
            <div class="col-md-2" style="padding-left: 0;">
              <h4 style="color:#5a5a5a;font-family: 'OpenSans-Bold',sans-serif; font-size: 13px;font-weight: bold;">CCTV SYSTEM</h4>
              <ul style="padding-top:17%;">
              <li class="a-slog"><a href="{{route('cctv')}}" style="color:#5a5a5a;font-size: 12px;">  IP CCTV System</a></li>
                <li class="a-slog"><a href="{{route('cctv')}}" style="color:#5a5a5a;font-size: 12px;">  Analoge CCTV System</a></li>
              </ul>
            </div>  
            <div class="col-md-3">
              <h4 style="color:#5a5a5a;font-family: 'OpenSans-Bold',sans-serif; font-size: 13px;font-weight: bold;">FIRE ALARM SYSTEM</h4>
              <ul style="padding-top:12%;">
                <li class="a-slog"><a href="{{route('firealarm')}}" style="color:#5a5a5a;font-size: 12px;">  Addressable Fire Alarm System</a></li>
                <li class="a-slog"><a href="{{route('firealarm')}}" style="color:#5a5a5a;font-size: 12px;">  Conventional Fire ALarm System</a></li>
              </ul>
            </div>
            <div class="col-md-3">
              <h4 style="color:#5a5a5a;font-family: 'OpenSans-Bold',sans-serif; font-size: 13px;font-weight: bold;">SOFTWARE SOLUTIONS</h4>
              <ul style="padding-top:12%;">
                <li  class="a-slog"><a href="{{route('customized')}}" style="color:#5a5a5a;font-size: 12px;">  Hotel PMS Software</a></li>
                <li class="a-slog"><a href="{{route('customized')}}" style="color:#5a5a5a;font-size: 12px;">  Customized Software Solutions</a></li>
                <li class="a-slog"><a href="{{route('customized')}}" style="color:#5a5a5a;font-size: 12px;">  SAP Services & Solutions</a></li>
              </ul>
            </div>
            <div class="col-md-1">
              <h4 style="color:#5a5a5a;font-family: 'OpenSans-Bold',sans-serif; font-size: 13px;font-weight: bold;">SUPPORT</h4>
              <ul style="padding-top:55%;">
              <li class="a-slog"><a href="reference" style="color:#5a5a5a;font-size: 12px;"> Project References </a></li>
                
              </ul>
            </div> 
            
            
            
        </div><hr style="margin-left: 10%;margin-right: 6%;margin-top: 5%;"><!-- end row -->
       <div class="row">
         <div class="col-md-12" style="padding-right: 10%;">
         <i class="fa fa-facebook circle"></i>
          <i class="fa fa-twitter circle"></i>
          @foreach(config('translatable.locales') as $lang=>$language)
          @if($lang!=app()->getlocale()) 
            @if($lang=='mm')
            <a href="{{route('lang.switch',$lang)}}"><img src="{{asset('images/myanmar.png')}}" style="width: 30px;height: 15px; float:right;margin-top:2%;margin-right: 10px;margin-left: 15px;"></a>
            @else
              <a href="{{route('lang.switch',$lang)}}"><img src="{{asset('images/english.png')}}" style="width: 30px;height: 15px; float:right;margin-top:2%;margin-right: 10px;margin-left: 15px;"></a>
              @endif
            @endif
            @endforeach
          <p style="font-size: 12px;color:#5a5a5a;float: right;margin-top: 2%;">Terms Privacy</p>
          <div class="row">
          <p style="font-size: 12px;float: right;color: #5a5a5a;"> &copy 2017.All Right Reserved.Ahtar Oo Co.,Ltd</p>
         </div>
         </div>
       </div>
      </div>  <!-- container -->

    </section>