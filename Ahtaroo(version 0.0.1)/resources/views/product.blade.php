
@extends('layout')
<style>
    .edit,.delete{
    background-color: #180aa3;
    color: white;
    padding: 5px 10px;
    border-radius: 25px;
    text-align: center; 
    text-decoration: none;
    display: inline-block;
    }
    .panel-heading{
        font-weight: bold;
        font-size: 20px;
        }
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Product</div>
                <div class="panel-body">
                    <form class="form-horizontal main" role="form" method="POST" action="insert_product" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Product Category Name</label>
                         <div class="col-md-6">
                          <select name="main" class="form-control">
                          <?php
                                    foreach ($Task as $value){
                                        $delete=$value->isDeleted;
                                     
                                      if($delete=='0')
                                      {
                        ?>
                               <option value="<?php echo $value->id;?>"><?php echo $value->CategoryName?></option>
                               
                               <?php
                            }
                        }
                            ?>
                            </select>
                            
                            </div>
                         </div>
                        <div class="form-group">
                            <label for="task" class="col-md-4 control-label">Product Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="product" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="end" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <textarea name='description' rows="5" cols="40" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="end" class="col-md-4 control-label">Product Photo</label>

                            <div class="col-md-6">
                                <input type="file" class="form-control" name="image">
                            </div>
                        </div>
                        <input type="hidden" name="delete" value="0">
                                       <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-primary">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
     
                           
@endsection