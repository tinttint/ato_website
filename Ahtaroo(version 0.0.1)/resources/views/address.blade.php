<style>
	.footer{
		background-color: #fff;
		padding-top: 2%;
	}
	address{
		font-family: 'mmrtext', sans-serif;
		font-size: 16px;
		color: #800000;
	}
	small{
		color: #800000;
		margin-left:45%;
    height: 2;
	}
</style>
 <!-- [FOOTER]
 ============================================================================================================================-->
 
<footer class="footer">
    
  <div class="container">
    <div class="row">
      <div class="col-md-9">
      	<address>No.B-9, Room-001, Shwe Kabar Housing, Mindama Road, Ward(3), Mayangone Township, Yangon. Tel: 951-664848 <br/>
        Building-16, Room-505 , MICT Park, Hlaing Township, Yangon . Tel: 951-2305193.<br/>
        Website: www.ahtaroo.com<br>
        Email&nbsp;&nbsp;&nbsp;&nbsp;: ahtaroocompany@gmail.com<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ato@ahtaroo.com</address>
      </div>
      <div class="col-md-3" > 
        <!--social-->
        
        <ul class="social">
          <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter "></i></a></li>
          <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
        </ul>
        
        <!--social end--> 
        
      </div>
    </div>
     
  </div>
  </footer>
  <div style="background-color: #f2f2f2;">
  <small class="text-center;">&copy;2017, QCST. All Rights Reserved.</small>
  </div>

 
 
 <!-- [/FOOTER]
 ============================================================================================================================-->