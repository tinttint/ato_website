<!DOCTYPE html>
<!--
++++++++++++++++++++++++++++++++++++++++++++++++++++++
AUTHOR : Vijayan PP
PROJECT :NIM
VERSION : 1.1
++++++++++++++++++++++++++++++++++++++++++++++++++++++
-->
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ahtar Oo Co.,Ltd</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- [ FONT-AWESOME ICON ] 
        =========================================================================================================================-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- [ PLUGIN STYLESHEET ]
        =========================================================================================================================-->
  <link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
  <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
  <!-- [ Boot STYLESHEET ]
        =========================================================================================================================-->
  
        <!-- [ DEFAULT STYLESHEET ] 
        =========================================================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/color/rose.css')}}"> 
  
</head>
  <style>

    .hr >.container
{
    padding: 6% 6% 1% 6%;
}

.hr i.fa {
  float: left;
  font-size:1.6em;
  width: 70px;
        height:70px;
        background:#fff;
        border-radius:50%;
        color:#000; 
        text-align: center;
        padding-top:25px;
        transition:all 200ms ease-in;
        -webkit-transition:all 200ms ease-in;
        -moz-transition:all 200ms ease-in;
        -ms-transition:all 200ms ease-in;
}
.hr .hr-detail {
  margin-left:90px;
}
.hr .hr-detail h4 {
font-family: 'Roboto Slab', serif;
}
.hr .hr-detail p {
  color: #fff;
        margin-top:10px;
        font-size:0.83em;
        color:#d9d9d9;
}

.hr:hover>i.fa
{
    background:#800000;
    color:#fff;
    box-shadow:2px 3px 5px 8px rgba(0,0,0,0.3);
    -webkit-box-shadow:2px 3px 5px 2px rgba(0,0,0,0.3);
    -moz-box-shadow:2px 3px 5px 2px rgba(0,0,0,0.3);
    -ms-box-shadow:2px 3px 5px 2px rgba(0,0,0,0.3);
}
.hr{
  background-color: #800000;
}
.nav a{
  font-family: 'times new roman', sans-serif;
  font-size: 16px;
}
.aboutus{
  padding-top:6%;
}
.aboutus{
  background-color: #800000;
}
p{
  font-family: 'mmrtext',sans-serif;
}

    </style>
</head>
<body >
<!-- [ LOADERs ]
================================================================================================================================--> 
    <div class="preloader">
    <div class="loader theme_background_color">
        <span></span>
      
    </div>
</div>
<!-- [ /PRELOADER ]
=============================================================================================================================-->
<!-- [WRAPPER ]
=============================================================================================================================-->
@include('navigation')
<div class="wrapper">
    
<img src="{{asset('images/up.png')}}" style="width: 50px; height: 50px;" onclick="topFunction()" id="myBtn" title="Go to top">

   <!-- [/NAV]
 ============================================================================================================================--> 
    
   <!-- [/software about]
 ============================================================================================================================--> 
<section class="main-heading" id="home">
       <div class="overlay">
           <div class="container" style="padding-top:10%;padding-bottom: 15%;">
           <div class="row">
            <div class="col-md-8" style="padding-right: 15%;">
              <h3 style="color: #a62a2a;font-size: 20px;padding-bottom: 10px;">Who We Are</h3>
              <p class="main" style="color: #484848;font-size: 14px;padding-top: 20px;text-align: justify;">Ahtar Oo Co., Ltd was initially established in 2009. Today, with better understanding of the importance of safety, security and the flow of information, the company transitioned into providing IT services and solution as a system integrator. We import product and sell to the end user directly and we provide installation, commissioning training, after sale service and maintenance. </p><p class="main" style="color: #484848;font-size: 14px;padding-top: 20px;text-align: justify;">The company’s core competencies includes the following but not limited to a strong ability in sourcing, well versed in the IT sector, and understanding of trading procedures and policies We put our customers’ needs and customers’ satisfactions ahead of ourselves while maintaining a level of reliability and creativity. This enabled the company to excel in the increasing competitive landscape.
              </p>
            </div>
               <div class="col-md-4" style="margin-left:-10px;">
                 <img src="{{asset('images/cctvsystem.png')}}" style="width: 80%;height: 100%;" class="img-responsive">
               </div>
  </div>
  </div>   
</div>

</section>

<section class="main-heading" id="home" style="background-color: #fcfcfc;">
       <div class="overlay">
           <div class="container" style="padding-top:10%;padding-bottom: 15%;">
           <div class="row">
           <div class="col-md-4" style="margin-left:-10px;">
                 <img src="{{asset('images/cctvsystem.png')}}" style="width: 80%;height: 100%;" class="img-responsive">
               </div>
            <div class="col-md-8" style="padding-right: 15%;">
              <h3 style="color: #a62a2a;font-size: 20px;padding-bottom: 10px;">Our Mission</h3>
              <p class="main" style="color: #484848;font-size: 14px;padding-top: 20px;text-align: justify;">Ahtar Oo Company is dedicated to building a long-term relationship with customers through quality products & services and wants to be recognized as a leading company in providing solutions in each related fields. </P><p class="main" style="color: #484848;font-size: 14px;padding-top: 20px;text-align: justify;">The company’s philosophy is to grow at a steady rate while sharing the successes with the employees and the customers alike</p>
            </div>
               
  </div>
  </div>   
</div>

</section>
       
 

 

 
@include('list')

</div>
 <script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>

<!-- [ /WRAPPER ]
=============================================================================================================================-->

  <!-- [ DEFAULT SCRIPT ] -->
    <script src="{{asset('library/modernizr.custom.97074.js')}}"></script>
      <script src="{{asset('library/jquery-1.11.3.min.js')}}"></script>
        <script src="{{asset('library/bootstrap/js/bootstrap.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>  
  <!-- [ PLUGIN SCRIPT ] -->
        <script src="{{asset('library/vegas/vegas.min.js')}}"></script>
  <script src="{{asset('js/plugins.js')}}"></script>
        <!-- [ TYPING SCRIPT ] -->
         <script src="{{asset('js/typed.js')}}"></script>
         <!-- [ COUNT SCRIPT ] -->
         <script src="{{asset('js/fappear.js')}}"></script>
       <script src="{{asset('js/jquery.countTo.js')}}"></script>
  <!-- [ SLIDER SCRIPT ] -->  
        <script src="{{asset('js/owl.carousel.js')}}"></script>
         <script src="{{asset('js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript" src="{{asset('js/SmoothScroll.js')}}"></script>
        
        <!-- [ COMMON SCRIPT ] -->
  <script src="{{asset('js/common.js')}}"></script>
</body>


</html>