<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=devidev-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Ahtar Oo Co.,Ltd</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
	<link rel="stylesheet" type="text/css" href="css/color/rose.css"> 
  <link rel="stylesheet" type="text/css" href="fonts/Open_Sans">
   <link href="fonts/Open_Sans/OpenSans-Bold" rel="stylesheet">  
  <script src="js/jssor.slider-25.0.7.min.js" type="text/javascript"></script>
    
    <style>
        
    
      .footer{
    background-color: #fff;
    padding-top: 2%;
  }
  
  p{
    font-family: 'mmrtext', sans-serif;
    color: #5a5a5a;
    font-size: 1em;
    text-align: justify;
  }
.logo{
  height: auto;
  background-color: #fcfcfc;
}
.who{
  background-color: #fff;
  height: auto;
}
.software{
  background-color: #fcfcfc;
  height: auto;
}
.product{
  background-color: #fff;
  height: auto;
}

.solution
{
    background:#fff;
}
.solution h3
{
    color:#5a5a5a;
}
#alhue:hover {
    content: url('{{asset('images/alhue.png')}}');
}
#ids:hover {
    content: url('{{asset('images/IDS.png')}}');
}
#fingertec:hover {
    content: url('{{asset('images/fingertec.png')}}');
}
#betech:hover {
    content: url('{{asset('images/betech.png')}}');
}
#gst:hover {
    content: url('{{asset('images/gst.png')}}');
}

  </style> 
</head>

<body>
	<!-- #region Jssor Slider Begin -->
    <script src="javascript/jquery-1.11.3.min.js" type="text/javascript"></script>
   
<div class="wrapper">
      @include('indexnavigation')
    <!-- Navbar -->
       


<img src="images/up.png" style="width: 50px; height: 50px;" onclick="topFunction()" id="myBtn" title="Go to top">
 	
   <section class="main-heading" id="home">
       <div class="overlay">
           <div class="container" style="padding-top:13%;">
           <div class="row">
            <div class="col-md-8">
              <h3 style="color: #a62a2a;font-size: 24px;padding-bottom: 10px;font-family: 'OpenSans-Bold',sans-serif; ">Ahtar Oo</h3>
              <p class="main" style="color: #484848;font-size: 2em;padding-top: 20px;">We Brings IT Solutions <br>For Your Satisfications.</p>
              <a href="#" style="color:#33ccff;font-weight: bold;font-size: 12px;">QCST</a>
              <a href="#" style="color:#b7b795;font-weight: bold;font-size: 12px;">AHTAR OO</a>
            </div>
               <div class="col-md-4" style="margin-left:-10px;">
                 <img src="{{asset('images/cctvsystem.png')}}" style="width: 80%;height: 100%;" class="img-responsive">
               </div>
  </div>
  </div>   
</div>
</section>
    
 <!-- [/MAIN-HEADING
 ============================================================================================================================-->
 <section class="logo">
 <div class="container">
   <div class="row">
   <div class="col-md-1"></div>
<div class="col-md-2" style="margin-left: 0;margin-top:1%;">
     <img id="alhue" src="{{asset('images/alhue_(grey).png')}}" alt="Alhue" class="img-responsive" style="width:80%;height: auto;padding-top: 10%;padding-bottom:10%;">
     </div>
     <div class="col-md-2">
<img id="ids" src="{{asset('images/IDS_(grey).png')}}" alt="Alhue" class="img-responsive" style="width:70%;height: auto;padding-top: 3%;padding-bottom:3%;margin-left: 5%;margin-top: 2%;">
   </div>
<div class="col-md-2">
<img id="fingertec" src="{{asset('images/fingertec_(grey).png')}}" alt="fingertec" class="img-responsive" style="width:100%;height: auto;padding-top: 10%;padding-bottom:10%;">
   </div>
   <div class="col-md-2">
<img id="betech" src="{{asset('images/betech_(grey).png')}}" alt="Betech" class="img-responsive" style="width:40%;height: auto;padding-top: 10%;padding-bottom:10%;margin-left: 30%; ">
   </div>
<div class="col-md-2">
<img id="gst" src="{{asset('images/gst_(grey).png')}}" alt="Alhue" class="img-responsive" style="width:80%;height: auto;padding-top: 10%;padding-bottom:10%;">
   </div>
      <div class="col-md-1"></div>

   </div>
   </div>
 </section>
 
 <section class="who">
 <div class="container" style="padding-bottom: 6%;">
   <div class="row">
     <div class="col-md-12" style="padding-left: 28%;padding-top: 6%;padding-bottom:1%;">
       <h2 style="color: #a62a2a; font-size: 1.5em;font-weight:bold;padding-bottom: 2%;">Who We Are</h2>
       <i class="fa fa-compass" aria-hidden="true" style="font-size: 24px;color:#484848;padding-right: 1%;"></i> 
       <i class="fa fa-compass" aria-hidden="true" style="font-size: 24px;color:#484848;padding-right: 1%;"></i> 
       <i class="fa fa-compass" aria-hidden="true" style="font-size: 24px;color:#484848;"></i> 

     </div>
     <div class="row">
     <div class="col-md-12" style="padding-left: 29%;padding-right: 29%;">
     <p>
       Ahtar Oo Co.,Ltd was initially established back in 2009 as a trading company, mainly specialized in food distribution company around the lower regions of Myanmar.
     </p>
     <p style="padding-top: 4%;">
       As the country becomes more reliant and gained more understanding of the importance of security and the follow of information,the company transitioned into providing a toiler-made security as well as IT solutions to the masses.
     </p>
      </div>
      </div>
   </div>
   </div>
 </section>
  <!-- [SOFTWARE]
 ============================================================================================================================-->
<section class="software text-center" id="two">
  <div class="container" style="padding-bottom: 6%;padding-top: 4%;">
   <div class="row">
     <div class="col-md-12" style="padding-top: 3%;padding-bottom:4%;">
       <h2 style="color: #a62a2a; font-size: 1.5em;font-weight:bold;padding-bottom: 2%;font-family: 'OpenSans-Bold',sans-serif; ">Software Solution</h2>
       </div>
       </div>
       <div class="row" style="padding-top:0;padding-left: 10%;padding-right: 10%;">
          <div class="col-sm-4 port-item margin-bottom">
            <div class="card" style="padding-top:11%;background-color:#fff;display:block;width:100%;border-radius:10px;margin:0 auto; border-style: solid;border-color: #f2f2f2;" >
                    
                <img class="img-responsive" src="{{asset('images/cctv.png')}}" style="width:360px; text-align:justify;display:block;margin-top: -34px;margin-left: 0;margin-right: 0;border-radius: 10px;padding: 25px 25px 25px 25px;" />
                                
            <div class="solution">
              <h3 style="font-family: 'OpenSans-Bold',sans-serif;"><a href="pms">Hotel PMS Software</a></h3>
              
            </div>           
    
          </div> <!-- /portfolio-item -->
          </div> <!-- /portfolio-item -->

          <div class="col-sm-4 port-item margin-bottom">
            <div class="card" style="padding-top:4%;background-color:#fff;display:block;width:100%;border-radius:10px;margin:0 auto;border-style: solid;border-color: #f2f2f2;" >
                    
                  <img class="img-responsive" src="{{asset('images/cctv.png')}}" style="width:360px; text-align:justify;display:block;margin-top: -34px;margin-left: 0;margin-right: 0;border-radius: 10px;padding: 25px 25px 25px 25px;" />
                                
                                <div class="solution">
              <h3 style="font-family: 'OpenSans-Bold',sans-serif;"><a href="{{route('customized')}}">Customized Software Solution</a></h3>
              
            </div>           
    
          </div> <!-- /portfolio-item -->
          </div>
          <div class="col-sm-4 port-item margin-bottom">
            <div class="card" style="padding-top:13%;background-color:#fff;display:block;width:100%;border-radius:10px;margin:0 auto;border-style: solid;border-color: #f2f2f2;" >
                    
                  <img class="img-responsive" src="{{asset('images/cctvsystem.png')}}" style="width:360px; text-align:justify;display:block;margin-top: -34px;margin-left: 0;margin-right: 0;border-radius: 10px;padding: 25px 25px 25px 25px;" />
                                
                                <div class="solution">
              <h3 style="font-family: 'OpenSans-Bold',sans-serif;"><a href="sap">SAP Services & Solutions</a></h3>
            </div>           
    
          </div> <!-- /portfolio-item -->
          </div> <!-- /portfolio-item -->
        </div> <!-- /row -->
       </div>
</section>
 <!-- [/SOFTWARE]
 ============================================================================================================================-->
 
  <!-- [PRODUCT]
 ============================================================================================================================-->
  <section class="product text-center" id="three" >
  <div class="container" style="padding-bottom: 5%;padding-top:4%;">
   <div class="row">
     <div class="col-md-12" style="padding-top: 5.9%;padding-bottom:1%;">
       <h2 style="color: #a62a2a; font-size: 1.5em;font-weight:bold;padding-bottom: 2%;font-family: 'OpenSans-Bold',sans-serif; ">Products</h2>
       </div>
       </div>
       <div class="row" style="padding-left: 10%;padding-right: 10%;">
          <div class="col-sm-4 port-item margin-bottom">
            <div class="card" style="padding-top:10%;background-color:#fff;display:block;width:100%;border-radius:10px;margin:0 auto; border-style: solid;border-color: #f2f2f2;" >
                    
                <img class="img-responsive" src="{{asset('images/cctv.png')}}" style="width:360px; text-align:justify;display:block;margin-top: -34px;margin-left: 0;margin-right: 0;border-radius: 10px;padding: 25px 25px 25px 25px;" />
                                
            <div class="solution">
              <h3 style="font-family: 'OpenSans-Bold',sans-serif; "><a href="{{route('accesscontrol')}}">Access Control System</a></h3>
              
            </div>           
    
          </div> <!-- /portfolio-item -->
          </div> <!-- /portfolio-item -->

          <div class="col-sm-4 port-item margin-bottom">
            <div class="card" style="padding-top:10%;background-color:#fff;display:block;width:100%;border-radius:10px;margin:0 auto;border-style: solid;border-color: #f2f2f2;" >
                    
                  <img class="img-responsive" src="{{asset('images/cctv.png')}}" style="width:360px; text-align:justify;display:block;margin-top: -34px;margin-left: 0;margin-right: 0;border-radius: 10px;padding: 25px 25px 25px 25px;" />
                                
                                <div class="solution">
              <h3 style="font-family: 'OpenSans-Bold',sans-serif; "><a href="{{route('cctv')}}">CCTV System</a></h3>
              
            </div>           
    
          </div> <!-- /portfolio-item -->
          </div>
          <div class="col-sm-4 port-item margin-bottom">
            <div class="card" style="padding-top:12%;background-color:#fff;display:block;width:100%;border-radius:10px;margin:0 auto;border-style: solid;border-color: #f2f2f2;" >
                    
                  <img class="img-responsive" src="{{asset('images/cctvsystem.png')}}" style="width:360px; text-align:justify;display:block;margin-top: -34px;margin-left: 0;margin-right: 0;border-radius: 10px;padding: 25px 25px 25px 25px;" />
                                
                                <div class="solution">
              <h3 style="font-family: 'OpenSans-Bold',sans-serif; "><a href="{{route('firealarm')}}">Fire Alarm System</a></h3>
              
            </div>           
    
          </div> <!-- /portfolio-item -->
          </div> <!-- /portfolio-item -->
        </div> <!-- /row -->
       </div>
</section><hr>
 <!-- [/PRODUCT]
 ============================================================================================================================-->

@include('list')

  <script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>

<!-- [ /WRAPPER ]
=============================================================================================================================-->

	<!-- [ DEFAULT SCRIPT ] -->
	<script src="library/modernizr.custom.97074.js"></script>
	<script src="library/jquery-1.11.3.min.js"></script>
        <script src="library/bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>	
	<!-- [ PLUGIN SCRIPT ] -->
        <script src="library/vegas/vegas.min.js"></script>
	<script src="js/plugins.js"></script>
        <!-- [ TYPING SCRIPT ] -->
         <script src="js/typed.js"></script>
         <!-- [ COUNT SCRIPT ] -->
         <script src="js/fappear.js"></script>
       <script src="js/jquery.countTo.js"></script>
	<!-- [ SLIDER SCRIPT ] -->  
        <script src="js/owl.carousel.js"></script>
         <script src="js/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/SmoothScroll.js"></script>
        
        <!-- [ COMMON SCRIPT ] -->
	<script src="js/common.js"></script>
  	<script>
			function onLoad_function(){
    			//document.getElementById('jssor_1').style.display = 'none';
				document.getElementById('jssor_1').style.height = 0;
			}
	</script>
</body>
	

</html>