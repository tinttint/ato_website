@extends('layout')
<style>
    .panel-heading{
        font-weight: bold;
        font-size: 20px;
        }
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Customer Registration</div>
                <div class="panel-body">
                    <form class="form-horizontal main" role="form" method="POST" action="insert_customer" enctype="multipart/form-data">
                        {{ csrf_field() }}
                         
                        <div class="form-group">
                        
                            <label for="cname" class="col-md-4 control-label">Customer Name</label>
                           

                            <div class="col-md-6">
                                <input id="cname" type="text" class="form-control" name="cname" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="btype" class="col-md-4 control-label">Business Type</label>

                            <div class="col-md-6">
                                <input id="btype" type="text" class="form-control" name="btype" required autofocus>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city" class="col-md-4 control-label">City</label>

                            <div class="col-md-6">
                            <select name="city" class="form-control">
                               
                               <option name="ygn">Yangon</option>
                               <option name="md">Mandalay</option>
                               
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="country" class="col-md-4 control-label">Country</label>

                            <div class="col-md-6">
                            <select name="country" class="form-control">
                               <option name="myn">Myanmar</option>
                               <option name="chi">Chinese</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="website" class="col-md-4 control-label">Website</label>

                            <div class="col-md-6">
                                <input id="website" type="text" class="form-control" name="website" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="end" class="col-md-4 control-label">Logo</label>

                            <div class="col-md-6">
                                <input type="file" class="form-control" name="logo">
                            </div>
                        </div>
                        <input type="hidden" name="delete" value="0">
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-primary">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
