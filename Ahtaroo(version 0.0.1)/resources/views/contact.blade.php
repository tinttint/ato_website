<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ahtar Oo Co.,Ltd</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
  <link rel="stylesheet" type="text/css" href="fonts/Open_Sans">
   <link href="fonts/Open_Sans/OpenSans-Bold" rel="stylesheet"> 
    <link href="fonts/Montserrat/Montserrat-Black" rel="stylesheet">  
  <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/color/rose.css')}}"> 
  
</head>
  <style>
  </style>
</head>
<body >
@include('navigation')
<div class="wrapper">
    
<img src="{{asset('images/up.png')}}" style="width: 50px; height: 50px;" onclick="topFunction()" id="myBtn" title="Go to top">
<section class="main-heading" id="home">
       <div class="overlay">
           <div class="container" style="padding-top:10%;">
           <div class="row">
            <div class="col-md-8" style="padding-right: 15%;">
              <h3 style="color: #a62a2a;font-size: 20px;padding-bottom: 10px;font-family:'Montserrat-Black',sans-serif;">Our Address</h3>
              <p class="main" style="color: #5a5a5a;font-size: 12px;padding-top: 20px;">No.B-9, Room-001, Shwe Kabar Housing, Mindama Road, Ward(3), Mayangone Township, Yangon. <br><i class="fa fa-phone" aria-hidden="true"></i> 951-664848<br><i class="fa fa-envelope" aria-hidden="true"></i>ahtaroocompany@gmail.com</p>
              <p class="main" style="color: #5a5a5a;font-size: 12px;padding-top: 20px;">Building-16, Room-505 , MICT Park, Hlaing Township, Yangon.<br><i class="fa fa-phone" aria-hidden="true"></i> 951-2305193<br><i class="fa fa-envelope" aria-hidden="true"></i>ato@ahtaroo.com</p>
            </div>
               <div class="col-md-4" style="margin-left:-10px;">
                 <img src="{{asset('images/cctvsystem.png')}}" style="width: 80%;height: 100%;" class="img-responsive">
               </div>
  </div>
  <div style="padding-top: 10%;">
    <p class="text-center" style="font-family: 'Open_Sans',sans-serif;font-size:12px;color:#5a5a5a;">CONTACT US<br><i class="fa fa-angle-down" aria-hidden="true" style="font-weight: bold;font-size: 2em;color: #000;"></i></p>
    </div>
  </div>   
</div>
</section>
    
 
<section class="text-center" style="background-color: #fcfcfc;">
  <div class="container" style="padding-bottom:3%;padding-top: 3%;padding-left:7%;">
   <div class="row">
     <div class="col-md-12" style="padding-top: 3%;padding-bottom:4%;">
       <h2 style="color: #5a5a5a; font-size: 20px;font-weight:bold;padding-bottom: 2%;font-family:'OpenSans-Bold',sans-serif; ">Tell Us What You Need</h2>
       </div>
       </div>
       <div>
         <form>
         <div class="text-center" style="padding-bottom: 2%;"> <input type="text" name="name" placeholder="Name"></div>
          <div class="text-center" style="padding-bottom: 2%;"> <input type="email" name="email" placeholder="Email"></div>
          <div><textarea rows="4" cols="23" placeholder="Message"></textarea></div>
           <div class="text-center"><input type="submit" class="btn btn-info" value="Send" style="padding-left:80px;padding-right: 80px;background-color: #a62a2a;"></div>
         </form>
       </div>
        </div>
       </div>
</section><hr>
 
 

 
@include('list')

</div>
 <script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>

<!-- [ /WRAPPER ]
=============================================================================================================================-->

  <!-- [ DEFAULT SCRIPT ] -->
    <script src="{{asset('library/modernizr.custom.97074.js')}}"></script>
      <script src="{{asset('library/jquery-1.11.3.min.js')}}"></script>
        <script src="{{asset('library/bootstrap/js/bootstrap.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>  
  <!-- [ PLUGIN SCRIPT ] -->
        <script src="{{asset('library/vegas/vegas.min.js')}}"></script>
  <script src="{{asset('js/plugins.js')}}"></script>
        <!-- [ TYPING SCRIPT ] -->
         <script src="{{asset('js/typed.js')}}"></script>
         <!-- [ COUNT SCRIPT ] -->
         <script src="{{asset('js/fappear.js')}}"></script>
       <script src="{{asset('js/jquery.countTo.js')}}"></script>
  <!-- [ SLIDER SCRIPT ] -->  
        <script src="{{asset('js/owl.carousel.js')}}"></script>
         <script src="{{asset('js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript" src="{{asset('js/SmoothScroll.js')}}"></script>
        
        <!-- [ COMMON SCRIPT ] -->
  <script src="{{asset('js/common.js')}}"></script>
</body>


</html>