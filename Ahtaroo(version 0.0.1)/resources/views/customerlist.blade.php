@extends('layout')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="bootstrap.min.css">

<style>
    .edit,.delete{
    background-color: #180aa3;
    color: white;
    padding: 5px 10px;
    border-radius: 25px;
    text-align: center; 
    text-decoration: none;
    display: inline-block;
    }
    .panel-heading{
        font-weight: bold;
        font-size: 20px;
        }
.addtask{
    float: right;
    margin-top: -40px;
    background-color: #180aa3;
    color: white;
    padding: 5px 5px;
    border-radius: 25px;
    text-align: center; 
    text-decoration: none;
    display: inline-block;
}
</style>
@section('content')
<div style="margin-top: -50px; padding: 0px 15px 0 15px;">
    <h3 style="color: #9f1d28;">Customer List</h3> 
    <a href="{{url('customer')}}" class='addtask'><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Customer</a>

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead style="background-color: #9f1d28; color:#fff;" >
                                    <tr>
                                        <th>No</th>
                                        <th>Customer Name</th>
                                        <th>Business Type</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Country</th>
                                        <th>Phone Number</th>
                                        <th>Email</th>
                                        <th>Website</th>
                                        <th>Logo</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                            
                               <?php
                                $i=1;

                                     foreach ($main as $value) {
                                      $delete=$value->isDeleted;
                                     
                                      if($delete=='0')
                                      {
                                                                           
                                    ?>

                                <tr>
                                <td><?php echo $i ?></td>
                                <td style="width: 10%;"><?php echo $value->cname;?></td>
                                <td style="width: 10%;"><?php echo $value->btype;?></td>
                                <td style="width: 10%;"><?php echo $value->address;?></td>
                                <td style="width: 10%;"><?php echo $value->city;?></td>
                                <td style="width: 10%;"><?php echo $value->country;?></td>
                                <td style="width: 10%;"><?php echo $value->phone;?></td>
                                <td style="width: 10%;"><?php echo $value->email;?></td>
                                <td style="width: 10%;"><?php echo $value->website;?></td>
                                <td style="width: 10%;"><?php echo $value->logo;?></td>
                                <td style="width: 100%; font-size: 14px;"><a href="editcustomer&<?php echo $value->id?>" class="edit">Edit</a>
                                   <a href="deletecustomer&<?php echo $value->id?>" class="delete">Delete</a>
                                </td>
                                </tr>
                                    
                                      
                                <?php
                                $i++;
                                 }
                                }
                           
                                ?>

                               </tbody>
                                </table>
                                </div>
                                </div>
@endsection