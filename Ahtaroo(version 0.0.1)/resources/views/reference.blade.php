<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ahtar Oo Co.,Ltd</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- [ FONT-AWESOME ICON ] 
        =========================================================================================================================-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- [ PLUGIN STYLESHEET ]
        =========================================================================================================================-->
  <link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
  <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
  
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/color/rose.css')}}"> 
</head>
<style>
th{
    font-size:14px;
}
td{
  font-size:12px;  
}
a{
    font-size: 12px;
    font-family:'OpenSans-Bold',sans-serif;
    font-weight: bold;
}
table{
    border:2px solid;
}
.table{
    padding-left:3%;
    padding-right: 3%;
}
th{
    padding:10%;
}
h3{
    font-size:20px;
    color:#5a5a5a;
    font-weight: bold;
    font-family: 'OpenSans-Bold',sans-serif;
    padding-top: 2%;
    padding-bottom: 3%;
    padding-left: 1%;
}
</style>
<body >
<!-- [ LOADERs ]
================================================================================================================================--> 
    <div class="preloader">
    <div class="loader theme_background_color">
        <span></span>
      
    </div>
</div>
<!-- [ /PRELOADER ]
=============================================================================================================================-->
<!-- [WRAPPER ]
=============================================================================================================================-->
<div class="wrapper">
  <!-- [NAV]
 ============================================================================================================================-->    
   <!-- Navigation
    ==========================================-->
    @include('navigation')
         
    <img src="{{asset('images/up.png')}}" style="width: 50px; height: 50px;" onclick="topFunction()" id="myBtn" title="Go to top">
    <script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>
   
<section class="white" id="contact" style="padding:1%;background-color: #fff;">
<div class="container">
    <div class="row" style="padding-top:6%;">
    <div class="col-sm-4 col-md-3 sidebar" style="padding-top: 0.5%;">
    <div class="mini-submenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </div>
    <div class="list-group">
        
        <a href="#" class="list-group-item toggleButton" data-target="div1">
            <i  class="font fa fa-angle-double-right text-primary"></i>All</a>
        <a href="#" class="list-group-item toggleButton" data-target="div2">
            <i class="font fa fa-angle-double-right text-primary"></i> Finger Print System
        </a>
        <a href="#" class="list-group-item toggleButton" data-target="div3">
            <i class="font fa fa-angle-double-right text-primary"></i> Face Regonization System
        </a>
        <a href="#" class="list-group-item toggleButton" data-target="div4">
            <i class="font fa fa-angle-double-right text-primary"></i> Hotel Door Lock System
        </a>
        <a href="#" class="list-group-item toggleButton" data-target="div5">
            <i class="font fa fa-angle-double-right text-primary"></i>Hotel Safe Box System 
        </a>
        <a href="#" class="list-group-item toggleButton" data-target="div6">
            <i class="font fa fa-angle-double-right text-primary"></i> Gate Control & Parking Management System 
        </a>
        <a href="#" class="list-group-item toggleButton" data-target="div7">
            <i class="font fa fa-angle-double-right text-primary"></i> CCTV System
        </a>
        <a href="#" class="list-group-item toggleButton" data-target="div8">
            <i class="font fa fa-angle-double-right text-primary"></i> Fire Alarm System
        </a>
    
        <a href="#" class="list-group-item toggleButton" data-target="div9">
            <i class="font fa fa-angle-double-right text-primary"></i> Hotel PMS Software
        </a>
        <a href="#" class="list-group-item toggleButton" data-target="div10">
            <i class="font fa fa-angle-double-right text-primary"></i> HR Management System </a>
        <a href="#" class="list-group-item toggleButton" data-target="div11">
            <i class="font fa fa-angle-double-right text-primary"></i> Performance Management System </a>
        <a href="#" class="list-group-item toggleButton" data-target="div12">
            <i class="font fa fa-angle-double-right text-primary"></i> Issue Ticketing System </a>
        <a href="#" class="list-group-item toggleButton" data-target="div13">
            <i class="font fa fa-angle-double-right text-primary"></i> Cardiac Surgery System </a>
        <a href="#" class="list-group-item toggleButton" data-target="div14">
            <i class="font fa fa-angle-double-right text-primary"></i> Attendance Management System </a>
        <a href="#" class="list-group-item toggleButton" data-target="div15">
            <i class="font fa fa-angle-double-right text-primary"></i> Employee Performance Reviewing System </a>
        <a href="#" class="list-group-item toggleButton" data-target="div16">
            <i class="font fa fa-angle-double-right text-primary"></i> SAP Services and Solutions
        </a>
    </div> 
        
        
           
</div>

<div class="col-sm-8 col-md-9 black" >

<div id="div1" class="toggleDiv" style="padding-top:1%">
  <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>All Customer Lists</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;          
          if($delete=='0')
            {                                      
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                           
               ?>        
    </tbody>
</table>
</div>
  </div>
</div>

<div id="div2" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Finger Print System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='1')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>

<div id="div3" class="toggleDiv" style="display:none">
<div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Face Recognition System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='2')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>

  <div id="div4" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Hotel Door Lock System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='3')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>

   <div id="div5" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Hotel Safe Box System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='4')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>
<div id="div6" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Gate Control & Parking Management System </h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='5')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>

<div id="div7" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>CCTV System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='6')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>
  <div id="div8" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Fire Alarm System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='7')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>
<div id="div9" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3> Hotel PMS Software</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='8')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>
<div id="div10" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>HR Management System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='9')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>

<div id="div11" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Performance Management System </h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='10')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>

<div id="div12" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Issue Ticketing System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='11')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>
  <div id="div13" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Cardiac Surgery System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='12')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>
<div id="div14" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Attendance Management System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='13')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>
<div id="div15" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>Employee Performance Review System</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='14')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>
<div id="div16" class="toggleDiv" style="display:none">
    <div style="border-style: solid;border :auto;border-width: 1px;border-color: #f2f2f2;">
  <h3>SAP Service & Solutions</h3>
  <div class="table">
    <table class="table table-bordered table-hover table-striped">
   <thead style="background-color: #fff; color:#484848;" >
         <tr>
            <th>No</th>
            <th>Customer Name</th>
            <th>Business Type</th>
            <th>City</th>              
        </tr>
    </thead>
    <tbody>
      <?php
        $i=1;
        foreach ($Task as $value) {
          $delete=$value->isDeleted;
          $product=$value->ProductID;          
          if($delete=='0')
            {                        
              if($product=='15')
              {             
        ?>
          <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $value->cname;?></td>
          <td><?php echo $value->btype;?></td>
          <td><?php echo $value->city;?></td>
          </tr>                  
               <?php
                   $i++;
                      }
                      }
                  }         
               ?>        
    </tbody>
</table>
</div>
  </div>
  </div>


  </div>
  </div>
  </div>
  </section><hr>
 
@include('list')
 </div>
 <script>
  $(function(){

   $('.toggleButton').click(function(){

         var target = $('#' + $(this).attr('data-target'));
         $('.toggleDiv').not(target).hide();
         target.show();
   });
});
 </script>

<!-- [ /WRAPPER ]
=============================================================================================================================-->

  <!-- [ DEFAULT SCRIPT ] -->
 <script src="{{asset('library/modernizr.custom.97074.js')}}"></script>
      <script src="{{asset('library/jquery-1.11.3.min.js')}}"></script>
        <script src="{{asset('library/bootstrap/js/bootstrap.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>  
  <!-- [ PLUGIN SCRIPT ] -->
        <script src="{{asset('library/vegas/vegas.min.js')}}"></script>
  <script src="{{asset('js/plugins.js')}}"></script>
        <!-- [ TYPING SCRIPT ] -->
         <script src="{{asset('js/typed.js')}}"></script>
         <!-- [ COUNT SCRIPT ] -->
         <script src="{{asset('js/fappear.js')}}"></script>
       <script src="{{asset('js/jquery.countTo.js')}}"></script>
  <!-- [ SLIDER SCRIPT ] -->  
        <script src="{{asset('js/owl.carousel.js')}}"></script>
         <script src="{{asset('js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript" src="{{asset('js/SmoothScroll.js')}}"></script>
        
        <!-- [ COMMON SCRIPT ] -->
  <script src="{{asset('js/common.js')}}"></script>


  
</body>


</html>