<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ahtar Oo Co.,Ltd</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- [ FONT-AWESOME ICON ] 
        =========================================================================================================================-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- [ PLUGIN STYLESHEET ]
        =========================================================================================================================-->
  <link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
  <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/color/rose.css')}}"> 
</head>
<body >
<!-- [ LOADERs ]
================================================================================================================================--> 
    <div class="preloader">
    <div class="loader theme_background_color">
        <span></span>
      
    </div>
</div>

<div class="wrapper">
  <!-- [NAV]
 ============================================================================================================================-->    
   <!-- Navigation
    ==========================================-->
    @include('navigation')
         
    <img src="{{asset('images/up.png')}}" style="width: 50px; height: 50px;" onclick="topFunction()" id="myBtn" title="Go to top">
    <script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>
   <section class="white" id="contact" style="padding:1%;background-color: #fff;">
<div class="container" style="padding: 5% 3% 12% 3%">
    <div class="row" style="padding-top: 3%;">
<div class="col-sm-2 col"></div>
<div class="col-sm-8 col-md-9 black">
<div class="row" style="padding-top: 20px;">
  <h2 style="font-family: 'OpenSans-Bold',sans-serif;font-size: 24px;color:#5a5a5a; ">Access Control System</h2>
  <div class="col-md-12">
    <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 14px;color:#5a5a5a;text-align: justify;">HRMS helps management and HR personals to manage and keep track of the appli.HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 18px;color:#5a5a5a;">Finger Print System</p>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 14px;color:#5a5a5a;text-align: justify;">HRMS helps management and HR personals to manage and keep track of the applicant information to get insight about candidate interview status, hiring status and vacancies.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 18px;color:#5a5a5a;">Face Regonization System</p>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 14px;color:#5a5a5a;text-align: justify;">HRMS ability to hold information on different areas of Employee in a central system helps Management and HR personals accurately record all Employee data and manage them effectively. Centralized data and a variety of reports also make tracking of Employee’s progress easier.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;" >
  <div class="col-md-12">
  <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 18px;color:#5a5a5a;">Hotel Door Lock System</p>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 14px;color:#5a5a5a;text-align: justify;">With HRMS, monitoring of employees’ in-time, out-time, total number of hours worked including overtimes and shift schedules can be done easily. Daily, Weekly and Monthly attendance reports and custom reports for individual employees, departments and location enable management to track employees’ attendance anytime. The system can also be integrated with Attendance Terminals such as Finger Print terminals and Face Recognition terminals.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 18px;color:#5a5a5a;" style="font-family: 'OpenSans-Bold',sans-serif;font-size: 14px;color:#5a5a5a;">Hotel Safe Box System</p>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 14px;color:#5a5a5a;text-align: justify;">All types of leaves across your organization can be recorded and monitored through analytic reports such as leave balances reports, leave applications reports etc.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 30px;">
  <div class="col-md-12">
  <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 18px;color:#5a5a5a;">Gate Control & Parking Management System</p>
  <div class="col-md-3"></div>
    <div class="col-md-9">
        <p style="font-family: 'OpenSans-Bold',sans-serif;font-size: 14px;color:#5a5a5a;text-align: justify;">This module manages to plan and keep track of training programs and employee progress to maintain the development skills for job productivity.</p>
    </div>
  </div>
</div>

</div>
<div class="col-sm-2"></div>




  </div>
  </div>
  </div>
  <hr>
  </section>

  

<!-- /access control system
=====================================================================================================-->

 
 <!-- [/Hotel Door Lock]
 ============================================================================================================================-->

 
 <!-- [/SERVICES]
 ============================================================================================================================-->
 
 
  
 
@include('list')
 
 <!-- [/FOOTER]
 ============================================================================================================================-->
 
 
 
</div>
 <script>
  $(function(){

   $('.toggleButton').click(function(){

         var target = $('#' + $(this).attr('data-target'));
         $('.toggleDiv').not(target).hide();
         target.show();
   });
});
 </script>

<!-- [ /WRAPPER ]
=============================================================================================================================-->

  <!-- [ DEFAULT SCRIPT ] -->
 <script src="{{asset('library/modernizr.custom.97074.js')}}"></script>
      <script src="{{asset('library/jquery-1.11.3.min.js')}}"></script>
        <script src="{{asset('library/bootstrap/js/bootstrap.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>  
  <!-- [ PLUGIN SCRIPT ] -->
        <script src="{{asset('library/vegas/vegas.min.js')}}"></script>
  <script src="{{asset('js/plugins.js')}}"></script>
        <!-- [ TYPING SCRIPT ] -->
         <script src="{{asset('js/typed.js')}}"></script>
         <!-- [ COUNT SCRIPT ] -->
         <script src="{{asset('js/fappear.js')}}"></script>
       <script src="{{asset('js/jquery.countTo.js')}}"></script>
  <!-- [ SLIDER SCRIPT ] -->  
        <script src="{{asset('js/owl.carousel.js')}}"></script>
         <script src="{{asset('js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript" src="{{asset('js/SmoothScroll.js')}}"></script>
        
        <!-- [ COMMON SCRIPT ] -->
  <script src="{{asset('js/common.js')}}"></script>


  
</body>


</html>