<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Ahtar Oo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/">Home</a></li>
        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin Task <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="maintype">ProductMainType</a></li>
            <li><a href="category">Category</a></li>
            <li><a href="product">Product</a></li>
            <li><a href="customer">Customer</a></li>
            <li><a href="productcustomer">Product and Customer</a></li>
            <li><a href="support">Product Support</a></li>
          </ul>
        </li>
       
      </ul>
      
    </div>
  </div>
</nav>
  