
@extends('layout')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="bootstrap.min.css">

<style>
    .edit,.delete{
    background-color: #180aa3;
    color: white;
    padding: 5px 10px;
    border-radius: 25px;
    text-align: center; 
    text-decoration: none;
    display: inline-block;
    }
    .panel-heading{
        font-weight: bold;
        font-size: 20px;
        }
.addtask{
    float: right;
    margin-top: -40px;
    background-color: #180aa3;
    color: white;
    padding: 5px 5px;
    border-radius: 25px;
    text-align: center; 
    text-decoration: none;
    display: inline-block;
}
</style>
@section('content')
<div class="container" style="margin-top: -50px;">
    <h3 style="color: #9f1d28;">Product List</h3> 
    <a href="{{ url('product') }}" class='addtask'><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Product</a>
     <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
      <thead style="background-color: #9f1d28; color:#fff;" >
                           <tr>
                      <th>No</th>
                      <th>Prodcut Category</th>
                      <th>Prodouct</th>
                      <th>Description</th> 
                      <th>Photo</th>   
                      <th>Actions</th>
                    </tr>
                   </thead>
                   <tbody>
                         
                        <?php
                           $i=1;
                                 
              foreach ($Task as $value) {
                  $delete=$value->isDeleted;
                 if($delete=='0')
                     {
                                                                        
                                    ?>

                                <tr>
                                <td><?php echo $i ?></td>
                                <td style="width: 20%;"><?php echo $value->CategoryName;?></td>
                                <td style="width: 15%;"><?php echo $value->ProductName;?></td>
                                <td style="width: 30%"><?php echo $value->Description;?></td>
                                <td style="width: 20%;"><?php echo $value->photo;?></td>
                                <td><a href="editproduct&<?php echo $value->id?>" class="edit">Edit</a>
                                    <a href="deleteproduct&<?php echo $value->id?>" class="delete">Delete</a>
                                </td>
                                </tr>
                                    
                                      
                                <?php
                                $i++;
                                 }
                             }
                           
                                ?>

                               </tbody>
                                </table>
                                </div>
                                </div>
@endsection