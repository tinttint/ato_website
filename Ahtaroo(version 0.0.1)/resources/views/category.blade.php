@extends('layout')
<style>
    .edit,.delete{
    background-color: #180aa3;
    color: white;
    padding: 5px 10px;
    border-radius: 25px;
    text-align: center; 
    text-decoration: none;
    display: inline-block;
    }
    .panel-heading{
        font-weight: bold;
        font-size: 20px;
        }
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Product Category Type</div>
                <div class="panel-body">
                    <form class="form-horizontal main" role="form" method="POST" action="insert_category">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Product Main Type Name</label>
                         <div class="col-md-6">
                          <select name="main" class="form-control">
                          <?php
                                    foreach ($Task1 as $value){
                                        $delete=$value->isDeleted;
                                     
                                      if($delete=='0')
                                      {
                        ?>
                               <option value="<?php echo $value->id;?>"><?php echo $value->MainTypeName?></option>
                               
                               <?php
                               
                           }
                        }
                            ?>
                            </select>
                            
                            </div>
                         </div>
                        <div class="form-group">
                            <label for="task" class="col-md-4 control-label">Category Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="category" required autofocus>
                            </div>
                        </div>

                        <input type="hidden" name="delete" value="0">
                                       <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-primary">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
       
    <div class="container">
                           <h3 style="color: #9f1d28;">Product Category List</h3> 

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead style="background-color: #9f1d28; color:#fff">
                                    <tr>
                                        <th>No</th>
                                        <th>Product Main Type Name</th> 
                                        <th>Category</th>   
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                            
                               <?php
                                $i=1;
                                 
                                    foreach ($Task as $value) {
                                      $delete=$value->isDeleted;
                                     
                                      if($delete=='0')
                                      {

                                                                           
                                    ?>

                                <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $value->MainTypeName;?></td>
                                <td><?php echo $value->CategoryName; ?></td>
                                <td><a href="editcategory&<?php echo $value->id ?>" class="edit">Edit</a>
                                    <a href="deletecategory&<?php echo $value->id ?>" class="delete">Delete</a>
                                </td>
                                </tr>
                                    
                                      
                                <?php
                                $i++;
                                 }
                             }
                           
                                ?>

                                
                                    
                                      
                                
                                </tbody>
                                </table>
                              
                                   
                                </div>
                                </div>
@endsection